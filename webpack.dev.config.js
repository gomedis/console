const path = require('path')
const commonConfig = require('./webpack.base')
const webpackMerge = require('webpack-merge')
const webpack = require('webpack')

console.log('Welcome to ' + process.env.NODE_ENV)

const publicPath = '/'
module.exports = function(env) {
  return webpackMerge(commonConfig(), {
    devtool: 'eval',
    // devtool: 'inline-source-map',
    //devtool: "source-map",
    entry: {
      app: [
        // activate HMR for React
        'react-hot-loader/patch',
        //'whatwg-fetch',
        'whatwg-fetch',

        `webpack-dev-server/client?http://${process.env
          .LOCAL_DEV_ADDRESS}:${process.env.DEVELOPMENT_PORT}`,
        // budle the client for webpack-dev-server
        // and connect to the provided endpoint

        'webpack/hot/only-dev-server',
        // bundle the client for hot reloading
        // only means to only hot reload for successful updates

        path.join(__dirname, 'client/index.js')
      ]
    },
    // output: {
    //   path: path.resolve(__dirname, "build"),
    //   filename: "[hash:5].[name].bundle.js",
    //   publicPath: "/"
    // },
    devServer: {
      disableHostCheck: true,
      hot: true,
      port: process.env.DEVELOPMENT_PORT,
      host: '0.0.0.0',
      historyApiFallback: true,
      noInfo: false,
      stats: {
        colors: true,

        assets: false,
        source: false,
        timings: true,
        hash: false,
        version: false,
        chunkModules: false,
        chunkOrigins: true
      },
      compress: true,
      publicPath: publicPath
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    ]
  })
}
