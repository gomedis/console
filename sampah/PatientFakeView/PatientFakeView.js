// // @flow
//
// import React, {Component} from 'react'
// import moment from 'moment'
// import {withRouter} from 'react-router'
//
// import {
//   Header,
//   Statistic,
//   Grid,
//   Button,
//   Dimmer,
//   Loader,
//   Segment,
//   Input,
//   Icon,
//   Popup,
//   Progress,
//   Checkbox
// } from 'semantic-ui-react'
// import {
//   createFragmentContainer,
//   createRefetchContainer,
//   graphql,
//   QueryRenderer
// } from 'react-relay'
// import {currentRelay} from '../../createRelayEnvironment'
// //import MainRenderer from '../../components/MainRenderer/MainRenderer'
// import ReactTableCustom from '../../components/ReactTableCustom/ReactTableCustom'
//
// type State = {
//   data: any,
//   isLoading: boolean,
//   pageIndex: number,
//   pageSize: number,
//   cursorStack: Array<any>,
//   isFirstRender: boolean,
//   paginate: string,
//   checked: any
// }
// type Props = any
// class PatientFakeView extends React.Component {
//   props: Props
//   state: State
//   constructor(props: Props) {
//     super(props)
//     //const lastPatient = this.getLast(this.props.viewer)
//     console.log('construct', this.getLastCursor(this.props.viewer))
//     this.state = {
//       data: [],
//       isLoading: false,
//       pageIndex: 0,
//       pageSize: 10,
//       cursorStack: ['', this.getLastCursor(this.props.viewer)],
//       isFirstRender: true,
//       paginate: 'first',
//       checked: {}
//     }
//   }
//
//   cb = error => {
//     if (error) {
//       console.log(error)
//     } else {
//     }
//   }
//
//   getLastCursor = viewer => {
//     const edges = viewer.allPatients.edges
//     if (edges.length > 0) {
//       return edges[edges.length - 1].node.id
//     }
//     return ''
//   }
//
//   handleSorting = sortingx => {
//     const orderBy = sortingx.map(sorting => {
//       return {
//         field: sorting.id,
//         direction: sorting.desc ? 'DESC' : 'ASC'
//       }
//     })
//     // orderBy.push({
//     //   field: 'createdAt',
//     //   direction: 'DESC'
//     // })
//
//     return orderBy
//   }
//
//   handleFilter = filtering => {
//     const xxx = {}
//
//     const filter = filtering.map(filterx => {
//       if (filterx.id === 'fullName') {
//         xxx[filterx.id] = {
//           like: `%${filterx.value}%`
//         }
//       } else if (filterx.id === 'username') {
//         xxx['user'] = {
//           username: {
//             like: `%${filterx.value}%`
//           }
//         }
//       } else if (filterx.id === 'id') {
//         xxx[filterx.id] = {
//           eq: `%${btoa('Patient:' + filterx.value)}%`
//         }
//       } else if (filterx.id === 'gender') {
//         xxx[filterx.id] = {
//           like: `${filterx.value}`
//         }
//       }
//     })
//
//     return xxx
//   }
//
//   isForward = pageIndex => {
//     if (pageIndex > this.state.pageIndex) {
//       return true
//     } else {
//       return false
//     }
//   }
//
//   getCursor = (isForward, pageIndex) => {
//     return this.state.cursorStack[pageIndex]
//   }
//
//   setCursorStack = pageIndex => {
//     if (pageIndex === 0) {
//       return ['']
//     } else {
//     }
//   }
//
//   handleTableChange = (state, instance) => {
//     //console.log('table changed state', state)
//     //console.log('table changed instance', instance)
//
//     if (this.state.isFirstRender) {
//       this.setState({
//         isFirstRender: false
//       })
//       return
//     }
//
//     const isForward = this.isForward(state.page)
//     //console.log(isForward? 'next': 'prev')
//
//     const cursor = this.getCursor(isForward, state.page)
//
//     //console.log(state.page)
//     this.setState({
//       isLoading: true,
//       pageSize: state.pageSize,
//       pageIndex: state.page,
//       paginate: isForward ? 'next' : 'prev'
//     })
//     //const cursor = this.getCursor(state.page)
//
//     const rV = {
//       count: state.pageSize,
//       cursor: cursor,
//       orderBy: this.handleSorting(state.sorting),
//       where: this.handleFilter(state.filtering)
//     }
//     this.props.relay.refetch(rV, null, this.cb, {
//       force: false
//     })
//   }
//
//   componentWillReceiveProps(nextProps) {
//     let currCursorStack = [...this.state.cursorStack]
//     if (this.state.paginate === 'prev') {
//       currCursorStack.pop()
//     }
//     const newCursor = this.getLastCursor(nextProps.viewer)
//     const cursors = [...currCursorStack, newCursor]
//     const buba = this.state.pageIndex === 0 ? ['', newCursor] : cursors
//     this.setState({
//       cursorStack: buba,
//       isLoading: false
//     })
//   }
//
//   handleTdClick = (state, rowInfo, column, instance) => {
//     return {
//       onClick: e => {
//         //console.log(column)
//         if (rowInfo && column.id !== 'checkbox') {
//           this.props.router.push(`/patient/${rowInfo.rowValues.id}`)
//         }
//         return
//       }
//     }
//   }
//
//   render() {
//     const columns = [
//       {
//         Header: 'Full Name',
//         id: 'fullName',
//         accessor: 'node.fullName' // String-based value accessors!
//       },
//       {
//         Header: 'Email',
//         id: 'username',
//         accessor: 'node.user.username', // String-based value accessors!
//         sortable: false
//         //filterRender: this.makePlaceholderFilter("Custom Text")
//       },
//       {
//         Header: 'Tgl Lahir',
//         id: 'dob',
//         accessor: 'node.dob',
//         Cell: props => {
//           return (
//            `${moment(props.value).format('L')} - ${moment().diff(props.value, 'years')}`
//           )
//         }
//       },
//       {
//         Header: 'Gender',
//         id: 'gender',
//         accessor: 'node.gender',
//         maxWidth: 100,
//         Cell: props => {
//           return (
//             <div style={{textAlign: 'center'}}>
//               <Popup
//                 size="mini"
//                 inverted
//                 position="top center"
//                 trigger={
//                   <Icon
//                     name={props.value === 'T1' ? 'man' : 'woman'}
//                     color={props.value === 'T1' ? 'violet' : 'pink'}
//                   />
//                 }
//                 content={props.value === 'T1' ? 'Pria' : 'Wanita'}
//               />
//             </div>
//           )
//         }
//       },
//       {
//         Header: 'Terdaftar',
//         id: 'createdAt',
//         accessor: 'node.createdAt',
//         Cell: props => {
//           return moment(props.value).fromNow()
//         }
//       }
//     ]
//     //console.log("RENDER prev", atob(this.state.prevCursor), 'next', atob(this.state.nextCursor))
//     //console.log('rendering:',this.state.cursorStack)
//     const {allPatients} = this.props.viewer
//     const count = this.props.viewer.agg.aggregations.count
//     const pages = count > 0 ? Math.ceil(count / this.state.pageSize) : 0
//     return (
//       <div style={{padding: '1em', height: '100%', background: '#eee'}}>
//         <ReactTableCustom
//           isLoading={this.state.isLoading}
//           //getTdProps={this.handleTdClick}
//           manual
//           loading={this.state.isLoading}
//           //onFakeChange={this.handleTableChange}
//           //filterable={true}
//           //pages={pages}
//           //page={this.state.pageIndex}
//           //fullAggregate={count}
//           showPageJump={false}
//           //showPageSizeOptions={true}
//           data={allPatients.edges}
//           //defaultPageSize={this.state.pageSize}
//           columns={columns}
//           className="-highlight"
//
//           //onChange={this._loadMore}
//         />
//       </div>
//     )
//   }
// }
//
// // const PatientViewRefetchContainer = createRefetchContainer(
// //   withRouter(PatientFakeView),
// //   graphql..experimental`
// //     fragment PatientFakeView_viewer on  Viewer{
// //       allPatients(
// //         first: $count
// //         after: $cursor
// //         orderBy: $orderBy
// //         where: $where
// //       ) {
// //         aggregations{
// //           count
// //         }
// //         edges{
// //           node{
// //             id
// //             createdAt
// //             fullName
// //             dob
// //             gender
// //
// //             user{
// //               id
// //               username
// //
// //             }
// //           }
// //         }
// //       }
// //       agg: allPatients(
// //         first: $count
// //         after: ""
// //         orderBy: $orderBy
// //         where: $where
// //       ) {
// //         aggregations{
// //           count
// //         }
// //       }
// //     }
// //   `,
// //   graphql..experimental`
// //     query PatientFakeViewRefetchQuery(
// //       $count: Int
// //       $cursor: String
// //       $orderBy: [PatientOrderByArgs]
// //       $where: PatientWhereArgs
// //     ) {
// //       viewer{
// //         id
// //         ...PatientFakeView_viewer
// //
// //       }
// //     }
// //   `
// // )
// //
// // const PatientQueryRenderer = (props: any) => (
// //   <QueryRenderer
// //     environment={environment}
// //     query={graphql..experimental`query PatientFakeViewQuery(
// //     $count: Int
// //     $cursor: String
// //     $orderBy: [PatientOrderByArgs]
// //     $where: PatientWhereArgs
// //     ) {
// //
// //       viewer {
// //         id
// //           ...PatientFakeView_viewer
// //         }
// //       }
// //    `}
// //     variables={{
// //       count: 10,
// //       cursor: null,
// //       orderBy: [
// //         {
// //           field: 'id',
// //           direction: 'DESC'
// //         }
// //       ]
// //       //where: null
// //     }}
// //     render={({error, props}) => {
// //       if (error) {
// //         console.error(error)
// //       }
// //       if (props) {
// //         console.log('mainprops', props.viewer)
// //         return <PatientViewRefetchContainer viewer={props.viewer} />
// //       }
// //
// //       return (
// //         <Dimmer active inverted>
// //           <Loader size="medium" inline inverted>Loading</Loader>
// //         </Dimmer>
// //       )
// //     }}
// //   />
// // )
// //
// // export default PatientQueryRenderer
