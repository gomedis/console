// @flow

import React, {Component} from 'react'
import ReactTable from 'react-table'
//import "./ReactTable.css"
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Message,
  Menu,
  Divider,
  Dropdown
} from 'semantic-ui-react'

export default class ReactTableCustom extends React.Component {
  static defaultProps: {}
  props: {
    pageSize: number,
    columns: Array<any>,
    data: any,
    isLoading: boolean,
    onFakeChange: Function,
    fullAggregate: number
  }
  state: {
    checkedId: Array<any>,
    cbIndeterminate: boolean,
    cbChecked: boolean,
    pageSize: number,
    page: number
  }

  constructor(props: any) {
    super(props)

    this.state = {
      checkedId: [],
      cbIndeterminate: false,
      cbChecked: false,
      pageSize: this.props.pageSize,
      page: 0
    }
  }

  makeColumns = (columns: Array<any>): Array<any> => {
    const newColumns = [
      {
        id: 'checkbox',
        Header: () =>
          <Checkbox
            indeterminate={this.state.cbIndeterminate}
            checked={this.state.cbChecked}
            onChange={(e, data) => {
              console.log('on changed headercb')
              if (this.state.cbIndeterminate) {
                this.turnOffAllCheckThisPage()
              } else {
                if (data.checked) {
                  this.turnOnAllCheckThisPage()
                } else {
                  this.turnOffAllCheckThisPage()
                }
              }
            }}
          />,
        filterable: false,
        sortable: false,
        maxWidth: 65,
        style: {textAlign: 'center'},
        //Cell: this.renderCheckboxColumn
        Cell: () => <Checkbox />
      },
      {
        Header: 'ID',
        id: 'id',
        maxWidth: 100,
        filterable: true,
        accessor: 'node.id', // String-based value accessors!
        Cell: props => atob(props.value)
      },
      ...columns
    ]

    return newColumns.map((column: any) => {
      if (!column.Filter) {
        
        column.Filter = this.makePlaceholderFilter(column.Header)
      }

      return column
    })
  }
  makePlaceholderFilter(placeholder: string) {
    return ({filter, onChange}: any) =>
      <Input
        //action={{ icon: 'close' }}
        icon={
          filter
            ? <Icon name="close" size="small" fitted link color="red" />
            : <Icon name="filter" size="small" fitted color="grey" />
        }
        placeholder={'Filter ' + placeholder}
        value={filter ? filter.value : ''}
        size="small"
        fluid
        transparent
        onChange={event => onChange(event.target.value)}
      />
  }
  _onChange = (state: any, instance: any) => {
    this.props.onFakeChange(state, instance)
  }

  turnOffAllCheckThisPage = () => {
    const dataTable = this.props.data.map(data => {
      return data.node.id
    })

    this.setState(prevState => ({
      cbIndeterminate: false,
      cbChecked: false,
      checkedId: this.toggleCheckAllPage('checknone', prevState.checkedId)
    }))
  }

  turnOnAllCheckThisPage = () => {
    this.setState(prevState => ({
      cbIndeterminate: false,
      cbChecked: true,
      checkedId: this.toggleCheckAllPage('checkall', prevState.checkedId)
    }))
  }

  toggleCheckAllPage = (action: any, checkedId: Array<string>) => {
    const dataTable: Array<string> = this.props.data.map(data => {
      return data.node.id
    })

    switch (action) {
      case 'checkall':
        return [...new Set([...dataTable, ...checkedId])]
      case 'checknone':
        return checkedId.filter(f => !dataTable.includes(f))
      //console.log('xxx', checkedId.filter(f => !dataTable.includes(f)))
      //return checkedId
      default:
        return checkedId
    }
  }

  renderCheckboxColumn = ({
    value,
    rowValues,
    row,
    column,
    index,
    viewIndex
  }: any) => {
    return (
      <div>
        <Checkbox
          fitted
          checked={this.rowIdChecked(rowValues.id)}
          onChange={(e, data) => {
            let abc = []
            if (data.checked) {
              abc = [...this.state.checkedId, rowValues.id]
            } else {
              const ids = this.state.checkedId
              const index = ids.indexOf(rowValues.id)
              abc = this.removeId(ids, index)
            }

            const dataTable = this.props.data.map(data => {
              return data.node.id
            })
            const curPageCheckedIds = abc.filter(f => dataTable.includes(f))
            const notChecked = dataTable.length - curPageCheckedIds.length
            //this.currentPageCheckedIds()
            this.setState({
              cbIndeterminate: notChecked > 0 && notChecked < dataTable.length,
              cbChecked: curPageCheckedIds.length === dataTable.length,
              checkedId: abc
            })
          }}
        />
      </div>
    )
  }

  currentPageCheckedIds = (): Array<string> => {
    return this.state.checkedId.filter(f => this.curPageIds().includes(f))
    //console.log(abc, dataTable)
  }
  curPageIds = (): Array<string> => {
    return this.props.data.map(data => {
      return data.node.id
    })
  }

  // calcCbChecked = (): boolean =>{
  //
  //   return curPageCheckedIds.length === this.curPageIds.length
  // }

  rowIdChecked = (rowId: string): boolean => {
    return this.state.checkedId.indexOf(rowId) >= 0
  }
  paginationButton = (props: any) => {
    const direction = props.children === 'Next' ? 'right' : 'left'
    return (
      <Button
        icon={`${direction} arrow`}
        labelPosition={direction}
        fluid
        content={props.children}
        disabled={props.disabled}
        onClick={props.onClick}
      />
    )
  }
  removeId = (list: Array<string>, index: number) => {
    return list.slice(0, index).concat(list.slice(index + 1))
  }
  render() {
    const columns = this.makeColumns(this.props.columns)

    console.log('tablerender', this.state.checkedId)
    //console.log(this.currentPageCheckedIds())
    //console.log('isequal',(['a','b'] == ['a','b']))
    return (
      <div
        style={{
          display: 'flex',
          flex: '1',
          flexDirection: 'column',
          minHeight: '100%'
        }}>

        <Menu attached="top" icon borderless size="large">

          <Popup
            position="bottom center"
            inverted
            trigger={
              <Menu.Item>
                <Icon.Group>
                  <Icon name="filter" />
                  <Icon corner name="remove" color="green" />
                </Icon.Group>
              </Menu.Item>
            }
            content="Clear all filter"
          />

          <Popup
            position="bottom center"
            trigger={<Menu.Item><Icon name="repeat" /></Menu.Item>}
            content="Refresh data"
            inverted
          />

          {this.currentPageCheckedIds().length > 0 &&
            <Popup
              position="bottom center"
              trigger={
                <Menu.Item color="red">
                  <Icon name="trash" color="red" />
                </Menu.Item>
              }
              content={`Delete ${this.currentPageCheckedIds().length} items`}
              inverted
            />}
          <Menu.Menu position="right" icon>
            {this.state.cbChecked &&
              this.props.data.length > 1 &&
              <Menu.Item>
                <small>
                  All
                  {' '}
                  <strong>{this.props.data.length}</strong>
                  {' '}
                  items on this page are selected.
                  {' '}
                  <u>
                    Select all
                    {' '}
                    <strong>{this.props.fullAggregate}</strong>

                    {' '}
                    items
                  </u>
                </small>
              </Menu.Item>}

          </Menu.Menu>
        </Menu>
        <Segment
          attached="bottom"
          style={{
            display: 'flex',
            flex: 2,
            flexDirection: 'column',
            justifyContent: 'center',
            padding: '0'
          }}>

          <Dimmer active={this.props.isLoading} inverted>
            <Loader size="medium">Loading</Loader>
          </Dimmer>
          <ReactTable
            {...this.props}
            //onChange={this._onChange}
            filterable
            columns={columns}

            // getTrProps={(state, rowInfo, column) => {
            //   // console.log('rowinfo',rowInfo)
            //   // console.log('roinfo.row',rowInfo.row)
            //   if (rowInfo === undefined) return {}
            //   return {
            //     style: {
            //       background: this.rowIdChecked(rowInfo.row.node.id)
            //         ? "#ffffeb"
            //         : "transparent"
            //     }
            //   }
            // }}

            //NextComponent={this.paginationButton}
            //PreviousComponent={this.paginationButton}
          />
        </Segment>

      </div>
    )
  }
}
