// @flow

import React from 'react'
import {IndexRoute, Route, Redirect} from 'react-router'
import {Dimmer, Loader} from 'semantic-ui-react/dist/es'
import NotFoundView from '../views/NotFoundView/NotFoundView'
import DashboardView from '../views/DashboardView/DashboardView'
import UserView from '../views/UserView/UserView'
import CustomerView from '../views/CustomerView/CustomerView'
import PatientView from '../views/PatientView/PatientView'
import PatientDetailView from '../views/PatientView/PatientDetailView'
import EmployeeView from '../components/Employee/EmployeeView'
import EmployeeFormView from '../components/Employee/EmployeeFormView'
import BranchView from '../views/BranchView/BranchView'
import BranchDetailView from '../views/BranchView/BranchDetailView'
import BranchFormView from '../views/BranchView/BranchFormView'
import ExamView from '../views/ExamView/ExamView'
import PanelView from '../views/PanelView/PanelView'
import AuthView from '../views/AuthView/AuthView'
import AppContainer from '../components/App/AppContainer'
import BranchDetailEmployeeView from '../views/BranchView/BranchDetailEmployeeView'
import BranchDetailGeneralView from '../views/BranchView/BranchDetailGeneralView'
import BranchListMenu from '../components/Branch/BranchListMenu'

export default (
  <div>
    <Route path="signup" initialScreen="signup" component={AuthView} />
    <Route path="login" initialScreen="login" component={AuthView} />
    <Route path="/" component={AppContainer}>
      <IndexRoute component={DashboardView} />
      <Route path="/patient">
        <IndexRoute component={PatientView} />
        <Route path=":id" component={PatientDetailView} />
      </Route>
      <Route path="/user" component={UserView} />
      <Route path="/exam" component={ExamView} />
      <Route path="/panel" component={PanelView} />
      <Route path="/customer" component={CustomerView} />
      <Route path="/employee">
        <IndexRoute component={EmployeeView} />
        <Route path='create' component={EmployeeFormView} mode='create'/>
        {/*<Route path=':id'>*/}
        {/*<IndexRoute component={EmployeeFormView} mode='read'/>*/}
        {/*<Route path='edit' component={EmployeeFormView} mode='edit'/>*/}
        {/*</Route>*/}
      </Route>
      <Route path="/branch" component={BranchView}>
        <IndexRoute component={BranchListMenu} />
        <Route path="/branch/create" component={BranchFormView} />
        <Route path="/branch/edit/:id" component={BranchFormView} />
        <Route path="/branch/:id" component={BranchDetailView}>
          <IndexRoute component={BranchDetailGeneralView} />
          <Route path="employee" component={BranchDetailEmployeeView} />
        </Route>
      </Route>
    </Route>
    <Route path="notfound" component={NotFoundView} />
    {/*<Redirect from="*" to="/notfound" />*/}
  </div>
)
