// @flow


const {
  Environment,
  Network,
  RecordSource,
  Store,
  ConnectionHandler,
  ViewerHandler
} = require('relay-runtime')

import type {Environment as EnvironmentType} from 'react-relay'


// temp fix https://github.com/facebook/relay/issues/1668
function update(store, payload) {
  const record = store.get(payload.dataID);
  if (!record) {
    return;
  }
  const serverViewer = record.getLinkedRecord(payload.fieldKey);
  record.setLinkedRecord(serverViewer, payload.handleKey);

  const root = store.getRoot();
  root.setLinkedRecord(serverViewer, payload.handleKey);
}


// Define a function that fetches the results of an operation (query/mutation/etc)
// and returns its results as a Promise:

function handlerProvider(handle) {
  switch (handle) {
    // Augment (or remove from) this list:
    case 'connection':
      return ConnectionHandler
    case 'viewer':
      return { update };
  }
  throw new Error(`handlerProvider: No handler provided for ${handle}`)
}

function fetchQuery(operation, variables, cacheConfig, uploadables) {
  //console.log(window, 'window!!!')

  const authToken: ?string = localStorage.getItem('medigo_auth_token')

  const bearer = (token: ?string): string =>{
    return token? 'Bearer ' + token  : ''
  }
  return fetch(__GRAPHCOOL_URL__, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      "Authorization": bearer(authToken)
    },
    body: JSON.stringify({
      query: operation.text,
      variables: variables
    })
  })
    .then(response => {
      return response.json()
    })
    .then(responseJson => {
      if (responseJson.errors) {
        return {
          data: null,
          errors: responseJson.errors
        }
      }
      return responseJson
    })
}

export let currentRelay = {
  reset: function(): void{
    //console.log('RESETING RELAY ENV')
    currentRelay.env = new Environment({
      handlerProvider,
      network: Network.create(fetchQuery),
      store: new Store(new RecordSource())
    })
  },
  env: null
}

currentRelay.reset()
