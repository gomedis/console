// @flow

import React, {Component} from 'react'
import LazilyLoad, {importLazy} from '../../components/LazilyLoad/LazilyLoad'
// import Auth0LockWrapper from '../../components/Auth0LockWrapper/Auth0LockWrapper'
import {currentRelay} from '../../createRelayEnvironment'
// import { browserHistory } from 'react-router'
import type {Router, Route} from 'react-router'
type Props = {
  router: Router,
  route: Route
}
export default class AuthView extends React.Component {
  props: Props
  constructor(props: Props) {
    super(props)
  }
  successCallback = (response: Response) => {
    console.log('SUCCESS CALLBACK CALLED')
    currentRelay.reset()
    this.props.router.push('/user')
    //window.location.pathname = '/'
    //console.log('oke', response)
  }

  render() {
    //console.log(this)

    return (
      <LazilyLoad
        modules={{
          Auth0LockWrapper: () =>
            importLazy(
              import('../../components/Auth0LockWrapper/Auth0LockWrapper')
            )
        }}>
        {({Auth0LockWrapper}) => {
          console.log('LOADED BROOOOOOOOO')
          return (
            <Auth0LockWrapper
              initialScreen={this.props.route.initialScreen}
              successCallback={this.successCallback}
              environment={currentRelay.env}
              //renderInElement={false}
            />
          )
        }}
      </LazilyLoad>
    )
  }
}
