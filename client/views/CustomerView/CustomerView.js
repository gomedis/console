// @flow

import React, {Component} from 'react'
import moment from 'moment'
import {withRouter} from 'react-router'
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Label
} from 'semantic-ui-react/dist/es'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import type {CustomerView_viewer} from './__generated__/CustomerView_viewer.graphql'

type State = {}
type Props = {
  relay: any,
  variables: any,
  viewer: CustomerView_viewer
}

class CustomerView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
  }

  render() {
    const {allCustomers} = this.props.viewer
    const columns = [
      {
        Header: 'Customer name',
        id: 'fullName',
        filterable: true,
        accessor: 'node.fullName',
        relayFilter: value => {
          return {
            fullName_starts_with: value
          }
        }
      },
      {
        Header: 'Email',
        id: 'email',
        sortable: false,
        filterable: true,
        accessor: 'node.user.email',
        relayFilter: value => {
          return {
            user: {
              email_starts_with: value
            }
          }
        }
      },
      {
        Header: 'Patient',
        id: 'patientcount',
        sortable: false,
        filterable: false,
        accessor: 'node.patients.count',
        maxWidth: 120
      }
    ]
    return (
      <div style={{padding: '1em', height: '100%'}}>
        <ReactTableRelay
          edgesType="Customer"
          nonPaginateCount={allCustomers.count}
          variables={this.props.variables}
          relay={this.props.relay}
          data={allCustomers.edges}
          columns={columns}
        />
      </div>
    )
  }
}
const query = graphql.experimental`query CustomerViewQuery(
$count: Int
$cursor: String
$orderBy: CustomerOrderBy
$filter: CustomerFilter
$skip: Int
) {
  viewer {
    id
    ...CustomerView_viewer
  }
}
`
const RefetchContainer = createRefetchContainer(
  CustomerView,
  graphql.experimental`fragment CustomerView_viewer on  Viewer{
    id
    user{
      email
    }
    allCustomers(
      first: $count
      after: $cursor
      orderBy: $orderBy
      filter: $filter
      skip: $skip
    ) {
      count
      pageInfo{
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
      edges{
        node{
          id
          fullName
          createdAt
          patients{
           count
          }
          user{
            id
            email
          }
        }
      }
    }
  }
  `,
  query
)

const CustomerQueryRenderer = () => {
  const variables = {
    count: 10,
    cursor: null,
    orderBy: null,
    filter: null,
    skip: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          return (
            <RefetchContainer variables={variables} viewer={props.viewer} />
          )
        }
        return <RelayQueryLoader />
      }}
    />
  )
}
export default CustomerQueryRenderer
