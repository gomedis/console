// @flow

import React, {Component} from 'react'
import moment from 'moment'
import {withRouter} from 'react-router'
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Dropdown,
  Select
} from 'semantic-ui-react/dist/es'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import type {ExamView_viewer} from './__generated__/ExamView_viewer.graphql'
type State = {}
type Props = {
  viewer: ExamView_viewer,
  relay: any,
  variables: any
}
class ExamView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
  }

  render() {
    const columns = [
      {
        Header: 'Name',
        id: 'name',
        accessor: 'node.name', // String-based value accessors!
        relayFilter: value => {
          return {
            name_starts_with: value
          }
        }
      }
    ]

    //console.log("RENDER prev", atob(this.state.prevCursor), 'next', atob(this.state.nextCursor))
    //console.log('rendering:',this.state.cursorStack)
    const {allExams} = this.props.viewer

    return (
      <div style={{padding: '1em', height: '100%'}}>
        <ReactTableRelay
          relay={this.props.relay}
          nonPaginateCount={allExams.count}
          variables={this.props.variables}
          edgesType="Exam"
          data={allExams.edges}
          columns={columns}
        />
      </div>
    )
  }
}

const query = graphql.experimental`query ExamViewQuery(
$count: Int
$cursor: String
$orderBy: ExamOrderBy
$filter: ExamFilter
$skip: Int
) {
  viewer {
    id
    ...ExamView_viewer
  }
}
`
const RefetchContainer = createRefetchContainer(
  withRouter(ExamView),
  graphql.experimental`
    fragment ExamView_viewer on  Viewer{
      allExams(
        first: $count
        after: $cursor
        orderBy: $orderBy
        filter: $filter
        skip: $skip
      ) {
        count
        pageInfo{
          startCursor
          endCursor
          hasNextPage
          hasPreviousPage
        }
        edges{
          node{
            id
            createdAt
            name

          }
        }
      }
    }
  `,
  query
)

const ExamQueryRenderer = (props: any) => {
  const variables = {
    count: 10,
    cursor: null,
    orderBy: null,
    filter: null,
    skip: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          return (
            <RefetchContainer variables={variables} viewer={props.viewer} />
          )
        }
        return <RelayQueryLoader />
      }}
    />
  )
}

export default ExamQueryRenderer
