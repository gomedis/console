// @flow

import React, {Component} from 'react'
import moment from 'moment'
import {withRouter, Link} from 'react-router'
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Label,
  Card,
  Divider,
  Container,
  Menu,
  Sidebar
} from 'semantic-ui-react/dist/es'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import BranchListMenu from '../../components/Branch/BranchListMenu'
import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import type { Children } from 'react';
type State = {}

type Props = {
  viewer: any,
  children: Children
}
class BranchView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
  }

  render() {
    return (
      <div>
        <Grid padded columns={1} stackable>
          <Grid.Row>
            <Grid.Column width={16} stretched>
              {React.cloneElement(this.props.children, {
                viewer: this.props.viewer
              })}
            </Grid.Column>

          </Grid.Row>

          {/*<Grid.Row>*/}
          {/*<Grid.Column width={16} stretched>*/}
          {/*<BranchListMenu viewer={this.props.viewer} />*/}
          {/*</Grid.Column>*/}
          {/*</Grid.Row>*/}
        </Grid>

      </div>
    )
  }
}
const query = graphql.experimental`query BranchViewQuery{
  viewer {
    id
    ...BranchListMenu_viewer
  }
}
`

const BranchQueryRenderer = propsx => {
  const variables = {
    count: 100,
    cursor: null,
    orderBy: null,
    filter: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      //variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          return (
            <BranchView
              variables={variables}
              viewer={props.viewer}
              {...props}
              {...propsx}
            />
          )
        }
        return (
          <Dimmer active inverted>
            <Loader size="medium" inline inverted>Loading</Loader>
          </Dimmer>
        )
      }}
    />
  )
}

export default withRouter(BranchQueryRenderer)
