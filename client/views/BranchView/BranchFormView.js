// @flow

import React, {Component} from 'react'
import {QueryRenderer, graphql} from 'react-relay'
import PropTypes from 'prop-types'
import BranchForm from '../../components/Branch/BranchForm'
import {currentRelay} from '../../createRelayEnvironment'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import {Segment} from 'semantic-ui-react/dist/es'

type Props = {
  router: any
}
export default class BranchFormView extends React.Component {
  props: Props
  renderEditForm = (branchId: string) => {
    return (
      <QueryRenderer
        environment={currentRelay.env}
        variables={{
          id: branchId
        }}
        query={graphql.experimental`
          query BranchFormView_Query($id: ID){
            viewer{
              Branch(id: $id){
                id
                city
                name
                location{
                  lat
                  lng
                }
                employees{edges{node{
                  id
                  fullName
                }}}
              }
            }
          }`}
        render={({error, props}) => {
          if (props) {
            return <BranchForm {...props} mode='edit' />
          }
          return <RelayQueryLoader text="Waiting" />
        }}
      />
    )
  }
  _onCreateComplete = () => {
    this.props.router.push('/branch')
  }
  renderCreateForm = () => {
    return (
      <BranchForm
        mode='create'
        onComplete={this._onCreateComplete}
        onCancel={this._onCreateComplete}
      />
    )
  }
  render() {
    const {params} = this.props.router
    //console.log('render',params.id ? 'EDIT MODE' + params.id : 'CREATE MODE')
    return (
      <Segment padded>
        {this.renderCreateForm()}
        {/*{params.id ? this.renderEditForm(params.id) : this.renderCreateForm()}*/}
      </Segment>
    )
  }
}
