// @flow

import React, {Component} from 'react'
import moment from 'moment'
import {withRouter} from 'react-router'

import {
  Button,
  Divider,
  Confirm
} from 'semantic-ui-react/dist/es'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'

import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import DeleteBranchMutation from '../../mutations/DeleteBranchMutation'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import EmployeeReactTableRelay from '../../components/Employee/EmployeeReactTableRelay'
import BranchForm from '../../components/Branch/BranchForm'
type Props = {
  branch: any,
  router: any
}

type State = {
  isEditing: boolean,
  confirmDeleteOpen: boolean
}

class BranchDetailGeneralView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
    this.state = {
      isEditing: false,
      confirmDeleteOpen: false
    }
  }
  _handleEditClick = () => {
    this.setState({
      isEditing: true
    })
  }
  _handleOnCompleteEdit = () => {
    this.setState({
      isEditing: false
    })
  }
  _handleDeleteConfirmed = () => {
    this.setState({confirmDeleteOpen: false})
    DeleteBranchMutation.commit(this.props.branch.id).then(() => [
      this.props.router.push('/branch')
    ])
  }
  _handleDeleteCancelled = () => {
    this.setState({confirmDeleteOpen: false})
  }

  render() {
    //console.log(this.props.branch)

    return (
      <div>
        {!this.state.isEditing
          ? <div>
              <pre>{JSON.stringify(this.props.branch, null, 2)}</pre>
              <Divider />
              <Button
                content="Edit"
                icon="pencil"
                color="yellow"
                size="large"
                onClick={this._handleEditClick}
              />
              <Confirm
                open={this.state.confirmDeleteOpen}
                onCancel={this._handleDeleteCancelled}
                onConfirm={this._handleDeleteConfirmed}
              />
              <Button
                content="Delete"
                icon="trash"
                floated="right"
                negative
                basic
                size="large"
                onClick={() => {
                  this.setState({
                    confirmDeleteOpen: true
                  })
                }}
              />
            </div>
          : <div>
              <BranchForm
                onComplete={this._handleOnCompleteEdit}
                onCancel={() => {
                  this.setState({isEditing: false})
                }}
                mode={'edit'}
                branch={this.props.branch}
              />
            </div>}

      </div>
    )
  }
}
//language=GraphQL
const query = graphql.experimental`
  query BranchDetailGeneralViewQuery($branchId: ID!)
  {
    node(id: $branchId){
      ...on Branch{
        id
        name
        city
        createdAt
        updatedAt
        employees{
          count
        }
      }
    }
  }
`

const GeneralQueryRenderer = queryProps => {
  const variables = {
    branchId: queryProps.params.id
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error('BRO', error.source)
          return <RelayQueryLoader text="PERMISSION ERROR" />
        }
        if (props) {
          //console.log(queryProps)
          return (
            <BranchDetailGeneralView
              variables={variables}
              branch={props.node}
              router={queryProps.router}
            />
          )
        }
        return <RelayQueryLoader />
      }}
    />
  )
}

export default withRouter(GeneralQueryRenderer)
