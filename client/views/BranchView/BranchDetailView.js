// @flow

import React, {Component} from 'react'
import moment from 'moment'
import {withRouter, Link, IndexLink} from 'react-router'
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Progress,
  Menu
} from 'semantic-ui-react/dist/es'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import type {Children} from 'react'

type MenuItemType = {
  to: string,
  as?: any,
  children?: Children
}

const MenuItem = (props: MenuItemType): React$Element<any> => {
  return (
    <Menu.Item
      as={props.as ? props.as : Link}
      activeClassName="active"
      to={props.to}
      name={props.name}
    />
  )
}
type Props = {
  params: any,
  children?: Children
}
class BranchDetailView extends React.Component {
  props: Props
  constructor(props: Props) {
    super(props)
    //const lastBranch = this.getLast(this.props.viewer)
  }

  render() {
    //console.log(this.props.router)
    const loc = `/branch/${this.props.params.id}`
    return (
      <Grid.Column>
        <Menu attached="top" tabular>
          <MenuItem name="General" to={loc} as={IndexLink} />
          <MenuItem name="Employee" to={loc + '/employee'} />
          <MenuItem name="Exam" to={loc + '/exam'} />
          <MenuItem name="Panel" to={loc + '/panel'} />
          <MenuItem name="Order" to={loc + '/order'} />
        </Menu>

        <Segment attached="bottom">
          <div style={{minHeight: '100%'}}>
            {this.props.children}
          </div>

        </Segment>
      </Grid.Column>
    )
  }
}

export default withRouter(BranchDetailView)
