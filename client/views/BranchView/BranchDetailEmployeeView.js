// @flow


import React, { Component } from "react"
import moment from "moment"
import { withRouter } from 'react-router';

import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Label
} from "semantic-ui-react"
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from "react-relay"
import {currentRelay} from "../../createRelayEnvironment"

import ReactTableRelay from "../../components/ReactTableRelay/ReactTableRelay"
import DeleteEmployeeMutation from '../../mutations/DeleteEmployeeMutation'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import EmployeeReactTableRelay from '../../components/Employee/EmployeeReactTableRelay'
type State = {}
type Props = {
  
}
class BranchDetailEmployeeView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
  }
  
  _handleDeleteClicked = (employeeIds) => {
    // employeeIds.forEach((employeeId)=>{
    //   DeleteEmployeeMutation.commit(
    //     employeeId
    //   )
    // })
  }
  
  render() {
    return (
      <EmployeeReactTableRelay {...this.props}/>
    )
  }
}
//language=GraphQL
const query = graphql.experimental`
  query BranchDetailEmployeeViewQuery(
  $first: Int
  $cursor: String
  $orderBy: EmployeeOrderBy
  $filter: EmployeeFilter
  $skip: Int

  ) {
    viewer {
      ...EmployeeReactTableRelay_viewer
    }
  }
`

const EmployeeQueryRenderer = (queryProps) => {
  const variables = {
    first: 10,
    cursor: null,
    orderBy:  null,
    filter: {
      branch:{
        id: queryProps.params.id
      }
    },
    skip: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({ error, props }) => {
        if (error) {
          console.error('BRO', error.source)
          return <RelayQueryLoader text='PERMISSION ERROR'/>
        }
        if (props) {
          return <BranchDetailEmployeeView variables={variables} viewer={props.viewer} />
        }
        return <RelayQueryLoader/>
        
      }}
    />
  )
}
export default withRouter(EmployeeQueryRenderer)
