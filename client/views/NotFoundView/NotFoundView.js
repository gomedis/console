// @flow

import React, {Component} from 'react'

import {Header, Icon} from 'semantic-ui-react/dist/es'

export default class NotFoundView extends React.Component{


  render () {
    return (
      <div>
        <Header as='h2' icon textAlign='center'>
          <Icon name='exclamation' circular />
          <Header.Content>
            404 NOT FOUND
          </Header.Content>
        </Header>
      </div>
    )
  }

}
