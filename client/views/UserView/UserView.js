// @flow

import React, {Component} from 'react'
import moment from 'moment'
import {withRouter} from 'react-router'

import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Label,
  Image
} from 'semantic-ui-react/dist/es'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'

import {currentRelay} from '../../createRelayEnvironment'
import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import DeleteUserMutation from '../../mutations/DeleteUserMutation'
import type {
  UserView_viewer 
} from './__generated__/UserView_viewer.graphql'
type State = {}
type Props = {
  viewer: UserView_viewer,
  relay: any,
  variables: any
}
class UserView extends React.Component {
  state: State
  props: Props
  constructor(props: Props) {
    super(props)
  }
  
  _handleDeleteClicked = (itemIds) => {
    console.log('delete Clicked', itemIds, this.props.viewer.id)
    itemIds.forEach((itemId)=>{
      DeleteUserMutation.commit(
        itemId,
      )
    })
    
    //this.props.relay.refetch();
  }
  
  render() {
    const {allUsers} = this.props.viewer
    const columns = [
      {
        id: 'avatar',
        filterable: true,
        accessor: 'node.avatarUrl', // String-based value accessors!
        width: 50,
        
        resizable: false,
        Cell: props => (
          <div>
          <Image src={props.value}  avatar centered />
          {/*<span>Username</span>*/}
      </div>
        )
      },
      {
        Header: 'Email',
        id: 'email',
        filterable: true,
        accessor: 'node.email', // String-based value accessors!
        relayFilter: value => {
          return ({
            email_starts_with: value
          })
        }
      },
      // {
      //   Header: "Patient Count",
      //   id: 'patientcount',
      //   sortable: false,
      //   filterable: false,
      //   accessor: "node.patient.aggregations.count",
      //   maxWidth: 120
      // },
      {
        Header: 'User Type',
        id: 'userType',
        sortable: false,
        className: '-center',
        filterable: false,
        accessor: 'node',
        Cell: ({value}) => {
          if (value.employee === null && value.customer === null)
            return <Label  size="small" content="None" />
          return value.employee !== null
            ? <Label  size="small">
                {value.employee !== null ? 'Employee' : 'Customer'}
              </Label>
            : <Label  size="small">
                {value.employee !== null ? 'Employee' : 'Customer'}
              </Label>
        }
      }
     
    ]
    const {commit} = DeleteUserMutation
    return (
      <div style={{padding: '1em', height: '100%'}}>
        <ReactTableRelay
          edgesType="User"
          commitDelete={commit}
          nonPaginateCount={allUsers.count}
          variables={this.props.variables}
          relay={this.props.relay}
          data={allUsers.edges}
          columns={columns}
        />
      </div>
    )
  }
}
const query = graphql.experimental`query UserViewQuery(
$count: Int
$cursor: String
$orderBy: UserOrderBy
$filter: UserFilter
$skip: Int
) {
  viewer {
    id
   
    ...UserView_viewer
  }
}
`
const RefetchContainer = createRefetchContainer(
  UserView,
  graphql.experimental`fragment UserView_viewer on  Viewer{
    id
    user{
      email
    }
    allUsers(
      first: $count
      after: $cursor
      orderBy: $orderBy
      filter: $filter
      skip: $skip
    )
    #@connection(key: "UserList_allUsers")
    {
     
      count
      edges{
        node{
          id
          email
          avatarUrl
          createdAt
          customer{
            id
          }
          employee{
            id
          }
        }
      }
    }
  }
  `,
  query
)

const UserQueryRenderer = () => {
  const variables = {
    count: 10,
    cursor: null,
    orderBy: "createdAt_DESC",
    filter: null,
    skip: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          return (
            <RefetchContainer variables={variables} viewer={props.viewer} />
          )
        }
        return <RelayQueryLoader/>
      }}
    />
  )
}
export default UserQueryRenderer
