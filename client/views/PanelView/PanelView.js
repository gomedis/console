// @flow

import React, {Component} from 'react'
import moment from 'moment'
import {withRouter} from 'react-router'
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Dropdown,
  Select,
  Label
} from 'semantic-ui-react/dist/es'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import CellDetailPopup from '../../components/Umum/CellDetailPopup'
import type {PanelView_viewer} from './__generated__/PanelView_viewer.graphql'
type State = {}
type Props = {
  relay: any,
  variables: any,
  viewer: PanelView_viewer
}
class PanelView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
  }

  render() {
    const columns = [
      {
        Header: 'Name',
        id: 'name',
        accessor: 'node',
        Cell: props => {
          return (
            <CellDetailPopup goto={`panel/${props.value.id}`}>
              {props.value.name}
            </CellDetailPopup>
          )
        },
        relayFilter: value => {
          return {
            name_starts_with: value
          }
        }
      },
      {
        Header: 'branchPanels',
        id: 'branchPanels',
        accessor: 'node.branchPanels',
        Cell: props => {
          return props.value.edges.map(({node}) => (
            <Label key={node.branch.name}>{node.branch.name}</Label>
          ))
        }
      }
    ]

    //console.log("RENDER prev", atob(this.state.prevCursor), 'next', atob(this.state.nextCursor))
    //console.log('rendering:',this.state.cursorStack)
    const {allPanels} = this.props.viewer

    return (
      <div style={{padding: '1em', height: '100%'}}>
        <ReactTableRelay
          relay={this.props.relay}
          nonPaginateCount={allPanels.count}
          variables={this.props.variables}
          edgesType="Panel"
          data={allPanels.edges}
          columns={columns}
        />
      </div>
    )
  }
}

const query = graphql.experimental`query PanelViewQuery(
$count: Int
$cursor: String
$orderBy: PanelOrderBy
$filter: PanelFilter
$skip: Int
) {
  viewer {
    id
    ...PanelView_viewer
  }
}
`
const RefetchContainer = createRefetchContainer(
  withRouter(PanelView),
  graphql.experimental`
    fragment PanelView_viewer on  Viewer{
      allPanels(
        first: $count
        after: $cursor
        orderBy: $orderBy
        filter: $filter
        skip: $skip
      ) {
        count
        pageInfo{
          startCursor
          endCursor
          hasNextPage
          hasPreviousPage
        }
        edges{
          node{
            id
            createdAt
            name
            branchPanels{
              count
              edges{node{
                branch{name}
              }}
            }

          }
        }
      }
    }
  `,
  query
)

const PanelQueryRenderer = (props: any) => {
  const variables = {
    count: 10,
    cursor: null,
    orderBy: null,
    filter: null,
    skip: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          return (
            <RefetchContainer variables={variables} viewer={props.viewer} />
          )
        }

        return <RelayQueryLoader />
      }}
    />
  )
}

export default PanelQueryRenderer
