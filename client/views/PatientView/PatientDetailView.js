// @flow

import React, { Component } from "react"
import moment from "moment"
import { withRouter } from 'react-router';
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
Progress
} from "semantic-ui-react"
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from "react-relay"
import {currentRelay} from "../../createRelayEnvironment"

type State = {
  data: any,
  isLoading: boolean,
  pageIndex: number,
  pageSize: number,
  isFirstRender: boolean,
  paginate: string
}

type Props = {
  patient: any
}
class PatientDetailView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
    //const lastPatient = this.getLast(this.props.viewer)
    this.state = {
      data: [],
      isLoading: false,
      pageIndex: 0,
      pageSize: 10,

      isFirstRender: true,
      paginate : 'first'

    }
  }

  render() {
   
    return (
      <div style={{height: '100%'}}>
        <pre>{JSON.stringify(this.props.patient, null, 4)}</pre>
      </div>
    )
  }
}

const PatientDetailViewContainer = createFragmentContainer(
  PatientDetailView,
  {
    patient:   graphql.experimental`
      fragment PatientDetailView_patient on Patient{
       
        customer{
          id
          fullName
          
          patients{
            edges{
              node{
                dob
                createdAt
                fullName
                gender
              }
            }
          }
     
        }
      }
    `
  }

)

const PatientDetailViewQuery =  (propsx: any) => (
  <QueryRenderer environment={currentRelay.env}
    query={graphql.experimental`query PatientDetailViewQuery ($id: ID!)
     {
       node(id: $id){
          ...PatientDetailView_patient

       }
     }
           `}
    variables={{
      id: propsx.router.params.id
    }}
    render={({ error, props }) => {
     //console.log(propsx)
      if (error) {
        console.error(error)
        return <pre>{JSON.stringify(error)}</pre>
      }
      if (props) {
        //console.log('asu:', props)
        //console.log("mainprops", props)
        //return <div>{JSON.stringify(props, null, 2)}</div>
        return <PatientDetailViewContainer patient={props.getPatient} />
      }

      return (
        <Dimmer active inverted>
          <Loader size="medium" inline inverted>Loading</Loader>
        </Dimmer>
      )

    }}
  />
)

export default withRouter(PatientDetailViewQuery)
