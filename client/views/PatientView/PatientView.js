// @flow

import React from 'react'
import moment from 'moment'
import {withRouter} from 'react-router'
import {Icon, Popup, Dropdown} from 'semantic-ui-react/dist/es'
import {
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import type {PatientView_viewer} from './__generated__/PatientView_viewer.graphql'
type State = {}
type Props = {
  viewer: PatientView_viewer,
  relay: any,
  variables: any
}
class PatientView extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
  }

  render() {
    const columns = [
      {
        Header: 'Full Name',
        id: 'fullName',
        accessor: 'node.fullName', // String-based value accessors!
        relayFilter: value => {
          return {
            fullName_starts_with: value
          }
        }
      },
      {
        Header: 'DOB - Age',
        id: 'dob',
        accessor: 'node.dob',
        Cell: props => {
          return (
            moment(props.value).format('L') +
            ' - ' +
            moment().diff(props.value, 'years') +
            ' thn'
          )
        }
      },
      {
        Header: 'Gender',
        id: 'gender',
        accessor: 'node.gender',
        //maxWidth: 150,
        relayFilter: value => {
          return {
            gender: value
          }
        },
        Cell: props => {
          return (
            <div style={{textAlign: 'center'}}>
              <Popup
                size="mini"
                inverted
                position="top center"
                trigger={
                  <Icon
                    name={props.value === 'T1' ? 'man' : 'woman'}
                    color={props.value === 'T1' ? 'blue' : 'pink'}
                  />
                }
                content={props.value === 'T1' ? 'Pria' : 'Wanita'}
              />
            </div>
          )
        },
        Filter: ({filter, onChange}) =>
          <div
            style={{
              width: 'inherit',
              position: 'absolute',
              background: 'yellow'
            }}>
            <Dropdown
              icon="filter"
              onChange={(event, data) => onChange(data.value)}
              fluid
              //defaultValue=''
              placeholder="Filter Gender"
              value={filter ? filter.value : ''}
              options={[
                {
                  value: '',
                  text: 'Semua',
                  key: 'semua'
                },
                {
                  value: 'T1',
                  text: 'Male',
                  key: 'male'
                },
                {
                  value: 'T2',
                  text: 'Female',
                  key: 'female'
                }
              ]}
            />
          </div>
      }
    ]

    //console.log("RENDER prev", atob(this.state.prevCursor), 'next', atob(this.state.nextCursor))
    //console.log('rendering:',this.state.cursorStack)
    const {allPatients} = this.props.viewer

    return (
      <div style={{padding: '1em', height: '100%'}}>
        <ReactTableRelay
          relay={this.props.relay}
          nonPaginateCount={allPatients.count}
          variables={this.props.variables}
          edgesType="Patient"
          data={allPatients.edges}
          columns={columns}
        />
      </div>
    )
  }
}

const query = graphql.experimental`query PatientViewQuery(
$count: Int
$cursor: String
$orderBy: PatientOrderBy
$filter: PatientFilter
  $skip: Int
) {
  viewer {
    id
    ...PatientView_viewer
  }
}
`
const RefetchContainer = createRefetchContainer(
  withRouter(PatientView),
  graphql.experimental`
    fragment PatientView_viewer on  Viewer{
      allPatients(
        first: $count 
        after: $cursor 
        orderBy: $orderBy
        filter: $filter
        skip: $skip
      ) {
        count
        pageInfo{
          startCursor
          endCursor
          hasNextPage
          hasPreviousPage
        }
        edges{
          node{
            id
            createdAt
            fullName
            dob
            gender
          }
        }
      }
    }
  `,
  query
)

const PatientQueryRenderer = (props: any) => {
  const variables = {
    count: 10,
    cursor: null,
    orderBy: null,
    filter: null,
    skip: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          return (
            <RefetchContainer variables={variables} viewer={props.viewer} />
          )
        }

        return <RelayQueryLoader />
      }}
    />
  )
}

export default PatientQueryRenderer
