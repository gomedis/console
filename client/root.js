// @flow

import 'react-table/react-table.css'
import 'react-datepicker/dist/react-datepicker.css'
import 'moment/locale/id'
import './main.css'
import React from 'react'
import {browserHistory, Router} from 'react-router'
import {QueryRenderer, graphql} from 'react-relay'
import Route from './routes/Route'
const Root = () => <Router history={browserHistory} routes={Route} />

export default Root
