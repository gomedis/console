// @flow

import React, {Component} from 'react'
import {withRouter, Link, IndexLink} from 'react-router'
import moment from 'moment'
import {currentRelay} from '../../createRelayEnvironment'
import type {TopMenu_viewer} from './__generated__/TopMenu_viewer.graphql'
import AccountMenuItem from './AccountMenuItem'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {
  Sidebar,
  Button,
  Menu,
  Input,
  Dropdown,
  Icon,
  Divider,
  Label
} from 'semantic-ui-react/dist/es'

type Props = {
  viewer: TopMenu_viewer,
  router: any
}

class TopMenu extends React.Component {
  props: Props
  constructor(props: Props) {
    super(props)
  }

  render() {
    return (
      <Menu fixed="top" color="grey" inverted className="topMenu">
        <Menu.Item className="topMenu-header">
          {/*<img src={logo}/>*/}
          <Label color="violet">
            MEDICAL
            <Label.Detail>GO</Label.Detail>
          </Label>
        </Menu.Item>
        <Menu.Item name="home" />
        <Menu.Item name="messages" />
        <Menu.Item name="friends" />
        <Menu.Menu position="right">
          <AccountMenuItem {...this.props} currentRelay={currentRelay} />
        </Menu.Menu>
      </Menu>
    )
  }
}

export default createFragmentContainer(
  withRouter(TopMenu),
  graphql.experimental`
    fragment TopMenu_viewer on Viewer{
      user{
        id
        email
        avatarUrl
        employee{fullName}
        customer{fullName}
      }
      id
    }
  `
)
