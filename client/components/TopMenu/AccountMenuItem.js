// @flow

import React, {Component} from 'react'
import {Dropdown, Menu, Icon} from 'semantic-ui-react/dist/es'
import {Link} from 'react-router'
import type {TopMenu_viewer} from './__generated__/TopMenu_viewer.graphql'


class AccountMenuItem extends React.Component {
  props: {
    viewer: TopMenu_viewer,
    router: any,
    currentRelay: any
  }

  _handleLogout = () => {
    localStorage.removeItem('medigo_auth_token')
    localStorage.removeItem('medigo_customer_id')
    this.props.currentRelay.reset()
    this.props.router.push('/')

    //window.location.pathname = '/'
  }

  renderMenuLogin = () => {
    return (
      <Menu.Item as={Link} to="login">
        <Icon name="sign in" />Login
      </Menu.Item>
    )
  }

  renderAccountDropDown = () => {
    const sayMyIdentity = (user): string => {
      if (user) {
        if (user.email && user.email.length > 0) {
          return user.email
        }
        if (user.employee) return user.employee.fullName
        if (user.customer) return user.customer.fullName
        return user.id
      } else {
        return 'Anonymous'
      }
    }
    const {user} = this.props.viewer
    return (
      <Dropdown item text={'Hello ' + sayMyIdentity(user)}>
        <Dropdown.Menu>
          <Dropdown.Item>My Profile</Dropdown.Item>
          <Dropdown.Item>Setting</Dropdown.Item>
          <Menu.Item onClick={this._handleLogout}>
            <Icon name="sign out" />Logout
          </Menu.Item>

        </Dropdown.Menu>
      </Dropdown>
    )
  }
  render() {
    const X = !localStorage.getItem('medigo_auth_token')
          ? this.renderMenuLogin
          : this.renderAccountDropDown
    return (
     <X/>
    )
  }
}

export default AccountMenuItem
