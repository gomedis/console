// @flow

import {QueryRenderer, graphql} from 'react-relay'
import localstorage from '../../__mocks__/localstorage'
import React from 'react'
import ReactDOM from 'react-dom'
import TopMenu from './TopMenu'
import {shallow, mount} from 'enzyme'
//import {it, describe} from 'jest';
import {currentRelay} from '../../createRelayEnvironment'
import renderer from 'react-test-renderer'

function QRenderer(TheComponent) {
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={graphql.experimental`query TopMenu_test_Query {
            viewer {

              ...TopMenu_viewer
            }
          }`}
      //variables={{}}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          console.log('props happening')
          return <TheComponent {...props} />
        }
        console.log('loading happening')
        return null
      }}
    />
  )
}

describe('<Foo />', () => {
  it('renders without crashing', done => {
    localStorage.setItem('medigo_auth_token', 'aaaa')

    const wrapper = mount(
      QRenderer(TopMenu)
    )
    setTimeout(() => {
      //console.log(wrapper.debug())
      expect(wrapper).toMatchSnapshot()
      done()
    }, 2000)
  })
})
