// @flow

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Button} from 'semantic-ui-react/dist/es'

import {currentRelay} from '../../createRelayEnvironment'
import ReactJsonSchemaForm, {
  radioWidget,
  dobWidget,
  dropdownWidget
} from '../../components/ReactJsonSchemaForm/ReactJsonSchemaForm'

import CreateBranchMutation from '../../mutations/CreateBranchMutation'
import UpdateBranchMutation from '../../mutations/UpdateBranchMutation'
import type {UpdateBranchMutationVariables} from '../../mutations/__generated__/UpdateBranchMutation.graphql'
import type {CreateBranchMutationVariables} from '../../mutations/__generated__/CreateBranchMutation.graphql'

type State = {
  isMutating: boolean,
}
type Props = {
  onComplete: Function,
  onCancel: Function,
  branch?: ?any,
  mode: 'edit' | 'create' | 'read'
}
class BranchForm extends React.Component {
  //props: Props
  state: State
  constructor(props: Props) {
    super(props)
    this.state = {
      isMutating: false,
    }
  }
  _handleSubmit = ({formData}: any) => {
    console.log(formData, 'formdatabos')
    if (this.state.isMutating) return
    this.setState({
      isMutating: true
    })
    const {id, ...restInput} = formData

    if (id){
      UpdateBranchMutation.commit({
       
       "input":{
        "clientMutationId": 'assasa',
          id,
          ...restInput
        }
      })
    }else{
      CreateBranchMutation.commit({
        
        input:{
          clientMutationId: 'assasa',
          ...restInput
        }
      })
    }
    this.props.onComplete()
    // const vars: UpdateOrCreateBranchMutationVariables = {
    //   input: {
    //     clientMutationId: 'assasa',
    //     create: {
    //       ...restInput
    //     },
    //     update: {
    //       id: id ? id : 'fake',
    //       ...restInput
    //     }
    //   }
    // }
    // console.log(vars, 'sending')
    // UpdateOrCreateBranchMutation.commit(vars)
   
      // .then(response => {
      //   if (this.props.onComplete) {
      //     this.props.onComplete()
      //   }
      // })
      // .catch(errors => {
      //   console.error(errors)
      // })
  }
  _handleCancel = () => {
    console.log('canceling')
    if (this.props.onCancel) this.props.onCancel()
  }

  renderSubmitButton = () => {
    return (
      <Button
        type="submit"
        size="large"
        positive
        disabled={this.state.isMutating}
        loading={this.state.isMutating}>
        {this.props.mode === 'create'? 'Simpan': 'Update'}
      </Button>
    )
  }
  renderCancelButton = () => {
    return (
      <Button
        type="button"
        size="large"
        color="yellow"
        basic
        disabled={this.state.isMutating}
        onClick={this._handleCancel}>
        Cancel
      </Button>
    )
  }
  render() {
    let formData
    const branch = this.props.branch
    if (branch) {
      formData = {
        id: branch.id,
        name: branch.name,
        city: branch.city
      }
    }
    return (
      <div>
        <ReactJsonSchemaForm
          //loading={this.state.isMutating}
          schema={branchSchema(this.props)}
          showErrorList={true}
          formData={formData}
          uiSchema={branchUiSchema}
          onSubmit={this._handleSubmit}
          liveValidate>
          {this.renderCancelButton()}
          {this.renderSubmitButton()}
        </ReactJsonSchemaForm>
      </div>
    )
  }
}

const branchSchema = props => {
  return {
    title: 'Branch Form',
    type: 'object',
    required: ['name', 'city'],
    properties: {
      name: {
        type: 'string',
        title: 'Branch Name'
      },
      city: {
        type: 'string',
        title: 'City'
      },
      lat: {
        type: 'string',
        title: 'Latitude'
      },
      lng: {
        type: 'string',
        title: 'Longitude'
      },
      address: {
        type: 'string',
        title: 'Address'
      }
    }
  }
}

const branchUiSchema = {}


export default BranchForm
