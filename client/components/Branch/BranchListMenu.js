// @flow weak

import React, {Component} from 'react'
import {
  Grid,
  Button,
  Dimmer,
  Loader,
  Label,
  Card,

} from 'semantic-ui-react/dist/es'
import {Link} from 'react-router'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import type {
  BranchListMenu_viewer
} from './__generated__/BranchListMenu_viewer.graphql'
import BranchListMenuCard from './BranchListMenuCard'
type Props ={
  viewer: BranchListMenu_viewer
}
class BranchListMenu extends React.Component {
  props: Props
  constructor(props: any) {
    super(props)
  }
  renderCreateBrachCard() {
    return (
      <Card>
        <Card.Content>
          <Card.Header>New Branch</Card.Header>
        </Card.Content>
        <Card.Content extra>
          <Button
            fluid
            color="green"
            as={Link}
            to={`/branch/create`}
            activeClassName="active">
            Create New Branch
          </Button>
        </Card.Content>
      </Card>
    )
  }

  render() {
    const {allBranches} = this.props.viewer
    return (
      <Card.Group itemsPerRow={3}>
        {allBranches.edges &&  allBranches.edges.map(branch => {
          return (
            <BranchListMenuCard key={branch.node.id}  branch={branch.node}/>
          )
        })}
        {this.renderCreateBrachCard()}
      </Card.Group>
    )
  }
}

export default createFragmentContainer(
  BranchListMenu,
  graphql.experimental`
    fragment BranchListMenu_viewer on Viewer{
      id
      allBranches(first: 100)
      @connection(key: "BranchList_allBranches", filters:["first"])  
      {
        edges{
          node{
            id
            ...BranchListMenuCard_branch
          }
        }
      }
    }
  `
)
