// @flow

import React, {Component} from 'react'
import {Button, Icon, Card} from 'semantic-ui-react/dist/es'
import {createFragmentContainer, graphql} from 'react-relay'
import {Link} from 'react-router'
import type {BranchListMenuCard_branch} from './__generated__/BranchListMenuCard_branch.graphql'
class BranchListMenuCard extends Component {
  props: {
    branch: BranchListMenuCard_branch
  }      
  render() {
    const {branch} = this.props
    const {employees} = branch
    return (
      <Card>
        <Card.Content>
          <Card.Header>{branch.name} {branch.city}</Card.Header>
          <Card.Meta>{branch.id}</Card.Meta>
          <Card.Meta>
            <Icon name="doctor" />
            {employees ? employees.count : '0'} Employees
          </Card.Meta>
        </Card.Content>
        <Card.Content extra>
          <div className="ui two buttons">
            <Button
              fluid
              icon="expand"
              basic
              color="green"
              as={Link}
              content="Detail"
              to={`/branch/${branch.id}`}
              activeClassName="active"
            />
            <Button basic color="red" icon="trash" content="Delete" />
          </div>
        </Card.Content>
      </Card>
    )
  }
}
const BranchListMenuCardContainer = createFragmentContainer(
  BranchListMenuCard,
  graphql.experimental`
    fragment BranchListMenuCard_branch on Branch{
      name
      id
      city
      createdAt
      employees{
        count
      }
    }
  `
)
export default BranchListMenuCardContainer
