// @flow

import React,{ Component } from "react"
import { withRouter } from "react-router"
import { debounce } from "lodash"

// using lastMount to prevent infinite instant reloading
const replace = debounce(
  (router: any, to: string) => {
    router.replace(to)
  },
  2000,
  {
    leading: true
  }
)

class RedirectOnMount extends React.Component {
  componentWillMount() {
    replace(this.props.router, this.props.to)
  }

  render() {
    return null
  }
}

export default withRouter(RedirectOnMount)
