// @flow

import React, {Component} from 'react'
import {Dropdown} from 'semantic-ui-react/dist/es'
import {graphql, createFragmentContainer, QueryRenderer} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
// import type {AllBranchWidget_viewer} from './__generated__/AllBranchWidget_Query.graphql'

type Props = {
  viewer: any,
  placeholder: ?string,
  onChange: Function,
  value?: ?any,
  options: any
}
type State = {
  firstRender: boolean
}

class AllBranchWidget extends React.Component {
  state: State
  props: Props
  constructor(props: Props) {
    super(props)
    this.state = {
      firstRender: true
    }
  }

  renderQueryRenderer = (propsx: Props) => {
    return (
      <QueryRenderer
        environment={currentRelay.env}
        query={graphql.experimental`
        query AllBranchWidget_Query{
          viewer{
            allBranches{
              edges{node{
                id
                name
                city
              }}
            }         
          }
        }
      `}
        render={({error, props}) => {
          if (props) {
            return <this.DropX {...propsx} {...props} />
          }
          return this.renderBiasa(propsx)
        }}
      />
    )
  }
  DropX = (props: any) => {
    const {allBranches} = props.viewer
    let dropdownOptions: any
    if (allBranches.edges) {
      dropdownOptions = allBranches.edges.map(({node}) => {
        return {
          key: node.id,
          text: `${node.name} (${node.city})`,
          value: node.id
        }
      })
    }
    return (
      <Dropdown
        fluid
        defaultOpen
        closeOnChange
        options={dropdownOptions}
        selection
        value={props.value}
        placeholder={'Pilih Branch'}
        onChange={(e, data) => props.onChange(data.value)}
        
      />
    )
  }
  renderBiasa = (props: Props) => {
    return (
      <Dropdown
        fluid
        options={[]}
        selection
        loading={!this.state.firstRender}
        text={props.options.name}
        placeholder={'Pilih Branch'}
        value={props.value}
        onClick={() => {
          this.setState({firstRender: false})
        }}
        onFocus={() => {
          this.setState({firstRender: false})
        }}
      />
    )
  }
  render() {
    if (this.state.firstRender) {
      return(
         this.renderBiasa(this.props)
      )
    } else {
      return(
        this.renderQueryRenderer(this.props)
      )
    }
  }
}

export default AllBranchWidget
