// @flow

import React, {Component} from 'react'
import {graphql, QueryRenderer, createRefetchContainer} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import {Dropdown, Input} from 'semantic-ui-react/dist/es'
import type {
  UserSearchWidget_viewer
} from './__generated__/UserSearchWidget_viewer.graphql'


const UserDropdown = (props: any) => {
  return (
    <Dropdown
      fluid
      selection
      minCharacters={0}
      multiple={false}
      search={true}
      options={[]}
      placeholder={'Select User'}
      {...props}
    />
  )
}
type State = {
  isFetching: boolean,
  searchQuery: string,
  value: string
}
type Props = {
  placeholder: string,
  value: string,
  relay: any,
  onChange: Function,
  options: any,
  viewer: UserSearchWidget_viewer
}
class UserSearchWidget extends React.Component {
  state: State
  props: Props
  constructor(props: Props) {
    super(props)
    this.state = {
      isFetching: false,
      searchQuery: '',
      value: this.props.value
    }
  }
  _onChange = (e: any, {value}: any) => {
    this.setState({
      value: value
    })
    this.props.onChange(value)
  }
  _onSearchChange = (e: any, value: string) => {
    if (this.state.isFetching) return
    this.setState({
      searchQuery: value,
      isFetching: true
    })
    const refetchVariables = fragmentVariables => ({
      filter: {
        email_starts_with: value,
        employee: null,
        customer: null
      }
    })
    this.props.relay.refetch(refetchVariables, null, () => {
      this.setState({
        isFetching: false
      })
    })
  }
  renderDropdown = (props: any) => {
    const {edges} = props.viewer.allUsers

    return (
      <UserDropdown
        options={edges.map(({node}) => {
          return {
            text: node.email,
            value: node.id,
            image: {
              avatar: true,
              src: node.avatarUrl
            }
          }
        })}
        value={this.state.value}
        //placeholder={this.props.placeholder}
        onChange={this._onChange}
        onSearchChange={this._onSearchChange}
        disabled={props.options.readOnly}
        loading={this.state.isFetching}
      />
    )
  }
  renderQueryLoading = () => {
    return <UserDropdown />
  }
  render() {
    const {edges} = this.props.viewer.allUsers
    console.log(this.props)
    if (this.props.options.readOnly) {
      return (
        <Input
          icon="lock"
          readOnly
          value={edges && edges[0] ? edges[0].node.email : undefined}
        />
      )
    }
    return this.renderDropdown(this.props)
  }
}
const query = graphql.experimental`
query UserSearchWidget_Query($filter: UserFilter){
  viewer{
    ...UserSearchWidget_viewer @arguments(filter: $filter)
  }
}`

const RefetchContainer = createRefetchContainer(
  UserSearchWidget,
  graphql.experimental`
    fragment UserSearchWidget_viewer on Viewer
    @argumentDefinitions(
      filter: {type: UserFilter, defaultValue: {
            employee: "",
            customer: ""
          }}
    ){
      allUsers(
        first: 10, filter: $filter,
        orderBy: createdAt_DESC
      )
      {edges{node{
        id
        email
        avatarUrl
      }}}
    }
  `,
  query
)
const UserSearchWidgetContainer = (propsx: any) => {
  let variables
  if (propsx.options.readOnly) {
    variables = {
      filter: {
        id: propsx.value
      }
    }
  } else {
    variables = {
      filter: {
        employee: null,
        customer: null
      }
    }
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (props) {
          return <RefetchContainer {...propsx} {...props} />
        }
        return propsx.options.readOnly
          ? <Input readOnly fluid icon='lock' value=' '/>
          : <UserDropdown disabled />
      }}
    />
  )
}

export default UserSearchWidgetContainer
