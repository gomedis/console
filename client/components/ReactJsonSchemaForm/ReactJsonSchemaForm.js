// @flow

import React, {Component, Children} from 'react'
import PropTypes from 'prop-types'
import Form from 'react-jsonschema-form'
import {
  Button,
  Checkbox,
  Dropdown,
  Divider,
  Input,
  Message,
  Label,
  Radio,
  Icon,
  Form as SForm
} from 'semantic-ui-react/dist/es'
import DatePicker from 'react-datepicker'
import moment from 'moment'

type Props = {
   children: Children
}
type State = {
 
}
class ReactJsonSchemaForm extends React.Component {
  props: Props
  state: State
  constructor(props: Props, context: any) {
    super(props, context)
  }
  render() {
    return (
      <Form
        //action=''
        //as={Form}
        //error
        //size={'tiny'}
        //schema={employeeSchema(this.props)}
        //uiSchema={employeeUiSchema}
        noHtml5Validate
        showErrorList={false}
        className={'ui tiny form error'}
        liveValidate
        //formData={formData.employee}
        FieldTemplate={SUIFieldTemplate}
        ErrorList={ErrorListTemplate}
        //onChange={() => console.log('changed')}
        //onSubmit={this.handleSubmit}
        //onError={() => console.log('errors')}
        {...this.props}>
        {this.props.children}
      </Form>
    )
  }
}


function SUIFieldTemplate(props) {
  const {
    id,
    classNames,
    label,
    help,
    required,
    description,
    errors,
    children,
    rawErrors
  } = props
  if (props.schema.type === 'object') {
    return children
  }
  
  return (
    <SForm.Field
      required={required}
      readOnly={props.readOnly}
      //width={8}
      error={rawErrors && rawErrors.length > 0}>
      <label
        style={{
          //width: 500
          //textAlign: 'right'
          //background:'yellow'
        }}>
        {label}
      </label>
      
      {children}
      {rawErrors &&
      <Label pointing color="red" basic>
        {rawErrors.map(error => (`${error}`))}
      </Label>}
    
    </SForm.Field>
  )
}


export function dobWidget(props: any) {
  return (
    <DatePicker
      selected={props.value ? moment(props.value) : undefined}
      onChange={date => props.onChange(date ? date.toISOString() : undefined)}
      minDate={moment().subtract(90, 'years')}
      maxDate={moment()}
      peekNextMonth={false}
      showMonthDropdown
      showYearDropdown
      dropdownMode="select"
      //className="ui input"
      placeholderText={props.placeholder}
      customInput={<DayPickerInput {...props.placeholder} />}
    />
  )
}

type DayPickerInputProps = {
  onClick: void,
  value: string,
  placeholder: string
}

class DayPickerInput extends React.Component {
  props: DayPickerInputProps
  constructor(props: DayPickerInputProps) {
    super(props)
  }
  
  render() {
    return (
      <Input
        fluid
        type="text"
        icon={<Icon name="calendar" link onClick={this.props.onClick} />}
        placeholder={this.props.placeholder}
        value={this.props.value}
        onChange={() => null}
        onClick={this.props.onClick}
        onFocus={this.props.onClick}
        readOnly
      />
    )
  }
}

export  function dropdownWidget(props: any) {
  return (
    <Dropdown
      
      placeholder={props.placeholder}
      //fluid
      selection
      options={enumToSemantic(props.options)}
      value={props.value}
      onChange={(e, data) => props.onChange(data.value)}
    />
  )
}

export function radioWidget(props: any) {
  console.log(props, 'radio')
  return (
    <SForm.Group inline={props.options.inline}>
      {props.options.enumOptions.map(opt => {
        return (
          <SForm.Field
            key={opt.value}
            control={Radio}
            label={opt.label}
            value={opt.value}
            checked={opt.value === props.value}
            onChange={(e, data) => props.onChange(data.value)}
          />
        )
      })}
    
    </SForm.Group>
  )
}

function enumToSemantic(JsonSchemaoptions) {
  return JsonSchemaoptions.enumOptions.map(option => {
    return {
      key: option.label,
      value: option.value,
      text: option.label
    }
  })
}

function ErrorListTemplate(props) {
  const {errors} = props
  const formattedErrors = errors.map((error, i) => {
    return  error.stack
  })
  console.log(errors, formattedErrors, 'ERROS')
  return (
    <Message
      error
      header="There was some errors with your submission"
      list={formattedErrors}
    />
  )
}
//ReactJsonSchemaForm.propTypes = {};
//ReactJsonSchemaForm.defaultProps = {};

export default ReactJsonSchemaForm
