// @flow

import React, {Component} from 'react'
//import ReactDOM from 'react-dom'
import ReactTable from 'react-table'
import './ReactTableRelay.css'
import {debounce, isEmpty} from 'lodash'
import {withRouter} from 'react-router'
import moment from 'moment'

import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Message,
  Menu,
  Divider,
  Dropdown,
  Label
} from 'semantic-ui-react/dist/es'

type State = {
  isFirstRender: boolean,
  cursorStack: Array<any>,
  orderBy: any,
  checkedIds: Array<any>,
  pageSize: number,
  pageIndex: number,
  filterBy: any,
  cbIndeterminate: boolean,
  cbChecked: boolean,
  isLoading: boolean,
  paginate: 'next' | 'prev'
}
type Props = {|
  commitDelete: Function,
  edgesType: string,
  variables: any,
  data: any,
  columns: Array<any>,
  relay: any,
  nonPaginateCount: number,
  onAddClick?: Function,
  getTrProps?: any,
  refreshData?: void
|}
type DefaultProps = {}
class ReactTableRelay extends Component{
 
  props: Props
  state: State
  nodex: any
  constructor(props: Props) {
    super(props)
    //console.clear()
    this.state = {
      isFirstRender: true,
      cursorStack: [null, this.getLastCursorFromEdges(this.props.data)],
      orderBy: this.props.variables.orderBy,
      filterBy: this.props.variables.filter,
      isLoading: false,
      checkedIds: [],
      cbIndeterminate: false,
      cbChecked: false,
      pageSize: this.props.variables.first,
      pageIndex: 0,
      paginate: 'next'
    }
  }
  
  makePlaceholderFilter(placeholder: string) {
    
    return ({filter, onChange}: any) =>
      <Input
        //action={{ icon: 'close' }}
        icon={
          filter
            ? <Icon name="close" size="small" fitted link color="red" />
            : <Icon name="filter" size="small" fitted color="grey" />
        }
        placeholder={'Filter ' + placeholder}
        value={filter ? filter.value : ''}
        fluid
        transparent
        onChange={event => onChange(event.target.value)}
      />
    }
   stateToVariables = (state: State): any => {
    const skip = state.pageSize * state.pageIndex
    return {
      first: state.pageSize,
      //cursor: state.cursorStack[state.pageIndex],
      cursor: null,
      skip: skip !== 0? skip: null,
      orderBy: state.orderBy,
      filter: state.filterBy
    }
   } 
  refetchData = (state: State): void => {
    const rV = this.stateToVariables(state)
    //console.log('refetching:', atob(rV.cursor))

    this.props.relay.refetch(
      rV,
      null,
      () => {
        //console.log('refetch!')
      },
      {
        force: false
      }
    )
  }

  handleDeleteClick = (e: any) => {
    const checkedIds = this.getCheckedIds(
      this.state.checkedIds,
      this.props.data
    )
    Promise.all(
      checkedIds.map(id => {
        return new Promise(resolve => {
          this.deleteByID(id).then(result => resolve(result))
        })
      })
    ).then(a => {
      //console.log('promise all complete', a)
      //this._refreshDataOnClick()
    })
  }

  deleteByID(id: string) {
    return this.props.commitDelete(id, this.stateToVariables(this.state))
  }

  componentWillReceiveProps(nextProps: any) {
    //const {checkedIds, cbChecked, cbIndeterminate} = this.calculateCheckBox(this.state.checkedIds, nextProps.data)
    //console.log('receivedProps', nextProps.viewer)
    this.setState({
      isLoading: false,

      ...this.calculateCheckBox(this.state.checkedIds, nextProps.data)
    })
  }

  _onPageChange = (pageIndex: number) => {
    const cursorStack = [...this.state.cursorStack]
    if (pageIndex > this.state.pageIndex) {
      cursorStack[pageIndex] = this.getLastCursorFromEdges(this.props.data)
    } else {
      cursorStack.pop()
    }

    this.setState(
      prevState => ({
        isLoading: true,
        pageIndex: pageIndex,
        cursorStack
      }),
      () => {
        this.refetchData(this.state)
      }
    )
    this.scrollToTop()
    console.log('onPageChange pageIndex', pageIndex)
  }

  _onPageSizeChange = (pageSize: number, pageIndex: number) => {
    this.setState(
      prevState => ({
        isLoading: true,
        cursorStack: [null],
        pageIndex: 0,
        pageSize: pageSize
      }),
      () => {
        this.refetchData(this.state)
      }
    )
    this.scrollToTop()
    console.log('onPageSize', 'pageSize', pageSize, 'pageIndex', pageIndex)
  }

  _onSortedChange = (newSorted: any, column: any, shiftKey: any) => {
    let orderBy = newSorted.map(sorting => {
      return `${sorting.id}_${sorting.desc ? 'DESC' : 'ASC'}`
    })
    console.log('orderBy: ', orderBy)
    console.log('orderByVar: ', this.props.variables.orderBy)
    if (orderBy.length < 1) {
      if (this.props.variables.orderBy) {
        orderBy = [this.props.variables.orderBy]
      } else {
        orderBy = ['createdAt_DESC']
      }
    }
    this.setState(
      prevState => ({
        isLoading: true,
        cursorStack: [null],
        pageIndex: 0,
        orderBy: orderBy[0]
      }),
      () => {
        this.refetchData(this.state)
      }
    )
    this.scrollToTop()
  }

  _onFilteredChange = (value: any, column: any) => {
    const xcolumn = this.makeColumns(this.props.columns)
    let v = {}
    const asu = value.map(({value, id}) => {
      let converted = {}

      const a = xcolumn.map(el => {
        if (id === el.id && el.relayFilter) {
          converted = el.relayFilter(value)
        }
      })
      Object.assign(v, converted)
    })

    console.log('onFiltered value', value)

    Object.assign(v, this.props.variables.filter)
    console.log('_.isEmpty(v)', isEmpty(v))
    this.setState(
      prevState => ({
        isLoading: false,
        cursorStack: [null],
        pageIndex: 0,
        filterBy: isEmpty(v) ? null : v
      }),
      () => {
        this.refetchData(this.state)
      }
    )
    //
  }

  _refreshDataOnClick = () => {
    console.log('resreshing data')
    this.setState(
      prevState => ({
        isLoading: true
      }),
      () => {
        this.refetchData(this.state)
      }
    )
  }

  getLastCursorFromEdges = (edges: Array<any>): ?string => {
    return edges.length > 0 ? edges[edges.length - 1].node.id : null
  }

  turnOffAllCheckThisPage = () => {
    const dataTable = this.props.data.map(data => {
      return data.node.id
    })

    this.setState(prevState => ({
      cbIndeterminate: false,
      cbChecked: false,
      checkedIds: this.toggleCheckAllPage('checknone', prevState.checkedIds)
    }))
  }

  turnOnAllCheckThisPage = () => {
    this.setState(prevState => ({
      cbIndeterminate: false,
      cbChecked: true,
      checkedIds: this.toggleCheckAllPage('checkall', prevState.checkedIds)
    }))
  }

  toggleCheckAllPage = (action: any, checkedIds: Array<string>) => {
    const dataTable: Array<string> = this.props.data.map(data => {
      return data.node.id
    })

    switch (action) {
      case 'checkall':
        return [...new Set([...dataTable, ...checkedIds])]
      case 'checknone':
        return checkedIds.filter(f => !dataTable.includes(f))
      //console.log('xxx', checkedId.filter(f => !dataTable.includes(f)))
      //return checkedId
      default:
        return checkedIds
    }
  }

  makeColumns = (columns: any) => {
    const newColumns = [
      {
        id: 'checkbox',
        Header: () =>
          <Checkbox
            indeterminate={this.state.cbIndeterminate}
            checked={this.state.cbChecked}
            onChange={(e, data) => {
              console.log('on changed headercb')
              if (this.state.cbIndeterminate) {
                this.turnOffAllCheckThisPage()
              } else {
                if (data.checked) {
                  this.turnOnAllCheckThisPage()
                } else {
                  this.turnOffAllCheckThisPage()
                }
              }
            }}
          />,
        filterable: false,
        sortable: false,
        width: 45,
        resizable: false,
        style: {textAlign: 'center'},
        Cell: this.renderCheckboxColumn
      },

      // {
      //   Header: 'Cursor',
      //   id: 'cursor',
      //   sortable: false,
      //   maxWidth: 230,
      //   accessor: 'cursor', // String-based value accessors!
      //   Cell: props => props.value,
      // },
      ...columns,
      {
        Header: 'Created At',
        id: 'createdAt',
        accessor: 'node.createdAt',
        Cell: props => {
          return moment(props.value).fromNow()
        }
      },
      {
        Header: 'ID',
        id: 'id',
        sortable: true,
        width: 240,
        accessor: 'node.id', // String-based value accessors!
        Cell: props => props.value,
        relayFilter: value => {
          return {
            id_starts_with: value
          }
        }
      }
    ]

    return newColumns.map(column => {
      if (!column.Filter) {
        column.Filter = this.makePlaceholderFilter(column.Header)
      }

      if (!column.relayFilter) {
        column.filterable = false
      }

      return column
    })
  }

  getCheckedIds = (checkedIds: Array<string>, data: any): Array<string> => {
    const abc = data.map(data => {
      return data.node.id
    })
    return checkedIds.filter(f => abc.includes(f))
    //console.log(abc, dataTable)
  }

  _getTrProps = (state: any, rowInfo: any, column: any) => {
    //if (!rowInfo) return {}

    const style = rowInfo && this.rowIdChecked(rowInfo.row.id)
      ? {
          style: {
            background: '#ffffeb'
          }
        }
      : {}

    const ext = this.props.getTrProps
      ? this.props.getTrProps(state, rowInfo, column)
      : {}

    return {
      ...ext,
      ...style
    }
  }

  renderCheckboxColumn = ({
    value,
    rowValues,
    row,
    column,
    index,
    viewIndex
  }: any) => {
    return (
      <Checkbox
        fitted
        checked={this.rowIdChecked(row.id)}
        onChange={(e, data) => {
          let abc = []
          //console.log(row.id)
          if (data.checked) {
            abc = [...this.state.checkedIds, row.id]
          } else {
            abc = this.removeId(this.state.checkedIds, row.id)
          }

          this.setState(this.calculateCheckBox(abc, this.props.data))

          //console.log('checked',data.checked, abc)
        }}
      />
    )
  }

  renderCustomPagination = (pages: number) => {
    return (
      <Menu attached="bottom" size="small">
        <Menu.Item
          disabled={this.state.pageIndex === 0}
          onClick={() => {
            if (this.state.pageIndex === 0) return
            this._onPageChange(this.state.pageIndex - 1)
          }}>
          Prev
        </Menu.Item>
        <Menu.Item
          disabled={this.state.pageIndex + 1 === pages}
          onClick={() => {
            if (this.state.pageIndex + 1 === pages) return
            this._onPageChange(this.state.pageIndex + 1)
          }}
          content="Next"
        />
        <Menu.Item>
          {`Halaman  ${this.state.pageIndex + 1} dari ${pages}`}
        </Menu.Item>
        <Menu.Item>
          {`Total data: ${this.props.nonPaginateCount}`}
        </Menu.Item>
      </Menu>
    )
  }

  calculateCheckBox = (abc: Array<string>, datax: any) => {
    const dataTable = datax.map(data => {
      return data.node.id
    })
    const curPageCheckedIds = abc.filter(f => dataTable.includes(f))
    const notChecked = dataTable.length - curPageCheckedIds.length
    //this.currentPageCheckedIds()
    if (dataTable.length < 1) {
      return {
        cbIndeterminate: false,
        cbChecked: false,
        checkedIds: abc
      }
    }
    return {
      cbIndeterminate: notChecked > 0 && notChecked < dataTable.length,
      cbChecked: curPageCheckedIds.length === dataTable.length,
      checkedIds: abc
    }
  }
  removeId = (list: Array<string>, id: string) => {
    const idx = list.indexOf(id)
    return list.slice(0, idx).concat(list.slice(idx + 1))
  }
  rowIdChecked = (rowId: string): boolean => {
    return this.state.checkedIds.indexOf(rowId) >= 0
  }

  _renderAddMenuItem = () => {
    return (
      <Popup
        position="bottom center"
        trigger={
          <Menu.Item
            onClick={() => {
              this.props.onAddClick()
            }}>
            <Icon name="add" />
          </Menu.Item>
        }
        content="Add data"
        inverted
      />
    )
  }
  _renderClearAllFilterMenuItem = () => {
    return (
      <Popup
        position="bottom center"
        inverted
        trigger={
          <Menu.Item>
            <Icon.Group>
              <Icon name="filter" />
              <Icon corner name="remove" color="green" />
            </Icon.Group>
          </Menu.Item>
        }
        content="Clear all filter"
      />
    )
  }

  _renderRefreshDataMenuItem = () => {
    return (
      <Popup
        position="bottom center"
        trigger={
          <Menu.Item onClick={this._refreshDataOnClick}>
            <Icon name="repeat" />
          </Menu.Item>
        }
        content="Refresh data"
        inverted
      />
    )
  }
  _renderSearchMenuItem = () => {
    return (
      <Menu.Item fitted="vertically">
        <Input
          transparent
          className="icon"
          icon="search"
          placeholder="Cari data ID, Nama, dll..."
        />
      </Menu.Item>
    )
  }
  _renderBranchDropdownMenuItem = () => {
    const options = [
      {key: 'all', icon: 'building', text: 'All Branch', value: 'all'},
      {key: 'mks', icon: 'building', text: 'Makassar', value: 'mks'},
      {key: 'smd', icon: 'building', text: 'Samarinda', value: 'smd'}
    ]
    return <Dropdown item placeholder="All Branch" options={options} />
  }
  scrollToTop() {
    //this.nodex.scrollTop = 0
  }
  componentDidMount() {
    // this.node = ReactDOM.findDOMNode(
    //   this.refs.myTable
    // ).childNodes[0].childNodes[2]
  }
  render() {
    const count = this.props.nonPaginateCount
    const pages = count > 0 ? Math.ceil(count / this.state.pageSize) : 0
    const checkedIds = this.getCheckedIds(
      this.state.checkedIds,
      this.props.data
    )

    return (
      <div
        style={{
          display: 'flex',
          flex: '1',
          flexDirection: 'column',
          minHeight: '100%'
        }}>
        <Menu attached="top" icon size="small" borderless>
          {this._renderSearchMenuItem()}
          {this._renderClearAllFilterMenuItem()}
          {this._renderAddMenuItem()}
          {this._renderRefreshDataMenuItem()}
          {checkedIds.length > 0 &&
            <Popup
              position="bottom center"
              trigger={
                <Menu.Item onClick={this.handleDeleteClick} color="red">
                  <Icon name="trash" color="red" />
                </Menu.Item>
              }
              content={`Delete ${checkedIds.length} items`}
              inverted
            />}

          {this._renderBranchDropdownMenuItem()}
        </Menu>

        <Segment
          attached
          style={{
            display: 'flex',
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            padding: '0'
          }}>
          <Dimmer active={this.state.isLoading} inverted>
            <Loader size="medium" inline="centered">Loading</Loader>
          </Dimmer>

          <ReactTable
            //ref={node => this.nodex = nodex}
            filterable
            minRows={0}
            //loading={this.state.isLoading}
            onPageChange={this._onPageChange}
            onPageSizeChange={this._onPageSizeChange}
            onFilteredChange={this._onFilteredChange}
            onSortedChange={this._onSortedChange}
            page={this.state.pageIndex}
            pageSize={this.state.pageSize}
            pages={pages}
            defaultPageSize={this.state.pageSize}
            {...this.props}
            showPagination={true}
            showPageJump={true}
            manual
            className="-highlight"
            columns={this.makeColumns(this.props.columns)}
            getTrProps={this._getTrProps}
          />

        </Segment>

        {this.state.cbChecked &&
          this.props.data.length > 0 &&
          <Menu attached borderless>
            <Menu.Item>
              <small>
                All
                {' '}
                <strong>{this.props.data.length}</strong>
                {' '}
                items on this page are selected.
                {' '}
                <u>
                  Select all
                  {' '}
                  <strong>{count}</strong>
                  {' '}
                  items
                </u>
              </small>
            </Menu.Item>
          </Menu>}
        {this.renderCustomPagination(pages)}
      </div>
    )
  }
}

export default ReactTableRelay
