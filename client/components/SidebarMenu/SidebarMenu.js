// @flow

import { Router, browserHistory, Link, IndexLink } from "react-router";
import React, { Component } from "react";
import {
  Sidebar,
  Button,
  Menu,
  Input,
  Dropdown,
  Icon,
  Divider,
  Label
} from "semantic-ui-react";

type Props = {};

type MenuItemType = {
  i: string,
  to: string,
  as?: any,
  children?: React$Element<any>
};

const MenuItem = (props: MenuItemType): React$Element<any> => {
  return (
    <Menu.Item
      //color="white"
      as={props.as ? props.as : Link}
      activeClassName="active"
      to={props.to}
    >
      {props.i && <Icon name={props.i} />}
      {props.children}
    </Menu.Item>
  );
};

export default class SidebarMenu extends React.Component {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Sidebar
          as={Menu}

          size='small'
          //icon='labeled'
          visible
          animation="overlay"
          width="thin"
          inverted
          //compact
          direction="left"
          color="black"
          vertical
          style={{
            background: '#424a5d',
            boxShadow: "none",
            transition: "initial",

          }}
        >
          <MenuItem i="dashboard" to="/" as={IndexLink}>
            Dashboard
          </MenuItem>
          <MenuItem i="user" to="/user">User</MenuItem>
          <MenuItem i="user" to="/customer">Customer</MenuItem>
          <MenuItem i="treatment" to="/patient">Patient</MenuItem>
          <MenuItem i='doctor' to="/employee">Employee</MenuItem>
          <MenuItem i='building' to="/branch">Branch</MenuItem>
          <MenuItem i='shopping basket' to="/order">Order</MenuItem>
          <MenuItem i='flask' to="/exam">Exam</MenuItem>
          <MenuItem i='list layout' to="/panel">Panel</MenuItem>
          <MenuItem i="graph bar" to="/analytics">Analytics</MenuItem>
          <MenuItem i="cogs" to="/config">Configuration</MenuItem>

        </Sidebar>
      </div>
    );
  }
}
//
// export default createFragmentContainer(SidebarMenu, {
//   viewer: graphql..experimental`
//     fragment SidebarMenu_viewer on Viewer{
//       user{
//         id
//         username
//       }
//     }
//   `
// })
