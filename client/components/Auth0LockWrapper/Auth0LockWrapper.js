// @flow

import React, {Component} from 'react'
import Auth0Lock from 'auth0-lock'
import {withRouter} from 'react-router'
import SigninUserMutation from '../../mutations/SigninUserMutation'
import CreateUserMutation from '../../mutations/CreateUserMutation'
import type {SignupUserInput} from '../../mutations/__generated__/CreateUserMutation.graphql'
declare var __AUTH0_CLIENT_ID__: string
declare var __AUTH0_DOMAIN__: string
import type {Environment} from 'react-relay'

type Props = {
  environment: Environment,
  initialScreen: 'login' | 'signup',
  successCallback: ?any
}

class Auth0LockWrapper extends React.Component {
  props: Props
  _lock: any
  constructor(props: Props) {
    super(props)
  }
  componentDidMount() {
    const selectFieldOptions = {
      closable: true,
      initialScreen: this.props.initialScreen,
      allowSignUp: false,
      autofocus: true,
      socialButtonStyle: 'small',
      params: {
        scope: 'email'
      },
      theme: {
        logo: require('../../assets/graphics/logo-purple.png'),
        primaryColor: '#6148a2'
      },
      languageDictionary: {
        title: 'MediGO'
      }
    }
    this._lock = new Auth0Lock(
      __AUTH0_CLIENT_ID__,
      __AUTH0_DOMAIN__,
      selectFieldOptions
    )
    this._lock.on('authenticated', this._handleAuth0Authenticated)
    this._lock.show()
  }
  _handleAuth0Authenticated = authResult => {
    console.log(authResult)
    window.localStorage.setItem(
      'medigo_auth_provider',
      authResult.idTokenPayload.sub
    )
    this._signInGraphQL(authResult)
  }

  _signInGraphQL(authResult) {
    const onErrorSignin = error => {
      const signInError = error.source.errors[0]
      if (signInError.code === 3022) {
        console.log('SIGNIN TO GRAPHQL FAILURE, AUTO SIGNUP!')
        this._signUpGraphQL(authResult)
      } else {
        console.log('SIGNIN FAILURE', signInError)
      }
    }
    //console.log(authResult)

    SigninUserMutation.commit(this.props.environment, authResult)
      .then(this._exitProcess)
      .catch(onErrorSignin)
  }

  _signUpGraphQL = authResult => {
    const onErrorSignUpGraphQL = error => {
      const signUpError = error.source.errors[0]
      console.log('SIGNUP FAILURE: ', signUpError)
      //DEAD END
    }
    const onSuccessSignUpGrapQL = response => {
      console.log('SIGNUP SUCCESS', response)
      this._signInGraphQL(authResult)
    }
    this._lock.getUserInfo(authResult.accessToken, (error, profile) => {
      if (!error) {
        console.log(profile)
        const input: SignupUserInput = {
          authProvider: {
            auth0: {
              idToken: authResult.idToken
            }
          },
          email: profile.email ? profile.email : '',
          avatarUrl: profile.picture
        }
        CreateUserMutation.commit(input)
          .then(onSuccessSignUpGrapQL)
          .catch(onErrorSignUpGraphQL)
      } else {
        console.error('GETUSERINFO ERROR', error)
      }
    })
  }

  _exitProcess = response => {
    //this._lock.hide()
    console.log('TYPEOF RESPONSE', typeof response)
    console.log('EXIT PROCESS', response)
    localStorage.setItem('medigo_auth_token', response.signinUser.token)
    localStorage.setItem('medigo_customer_id', response.signinUser.user.id)
    const {successCallback} = this.props
    if (successCallback) successCallback(response.signinUser)
    
  }
  componentWillUnmount() {
    this._lock.hide()
  }

  render() {
    return <div />
  }
}

export default withRouter(Auth0LockWrapper)
