// @flow

import React from 'react'
import {QueryRenderer, createFragmentContainer, graphql} from 'react-relay'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import {currentRelay} from '../../createRelayEnvironment'
import App from './App'

//language=GraphQL
const query = graphql.experimental`query AppContainerQuery {
  viewer {
    ...TopMenu_viewer
  }
}
`


const AppContainerQueryRenderer = (containerProps: any) => {
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={{}}
      render={({error, props}) => {
        if (error) {
          console.error(error)
        }
        if (props) {
          return <App {...containerProps} {...props} />
        }
        return <RelayQueryLoader />
      }}
    />
  )
}


export default AppContainerQueryRenderer
