// @flow

import React, {Component} from 'react'
import SidebarMenu from '../SidebarMenu/SidebarMenu'
import TopMenu from '../TopMenu/TopMenu'
import {
  Sidebar,
  Menu,
  Icon,
  Button,
  Rail,
  Segment,
  Embed,
  Input,
  Container,
  Grid,
  Header,
  Dimmer,
  Loader
} from 'semantic-ui-react/dist/es'
type Props = {
  viewer: any,
  children: any
}
export default class App extends React.Component {
  props: Props
  constructor(props: Props) {
    super(props)
  }

  render() {
    return (
      <div className='main-app-wrapper'>
        <TopMenu viewer={this.props.viewer} />
        <Sidebar.Pushable>
          <SidebarMenu />
          <Sidebar.Pusher>
            <div className='container-outer'>
              <div className='container-inner'>
                {this.props.children}
              </div>
            </div>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    )
  }
}
