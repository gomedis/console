// @flow

import React, {Component} from 'react'
import {Header, Icon, Grid, Segment, Divider} from 'semantic-ui-react/dist/es'
import EmployeeForm from '../../components/Employee/EmployeeForm'
type Props = {
  route: any,
  router: any
}
class EmployeeFormView extends React.Component {
  props: Props
  constructor(props: Props) {
    super(props)
  }
  render() {
    console.log(this.props)
    return (
      <Grid padded>
        <Grid.Row>
          <Grid.Column>
            
            <Segment stacked padded>
              <Header as="h2">
                <Icon name="doctor" />
                <Header.Content>
                  Manage Employee
                  <Header.Subheader>
                    Mode {this.props.route.mode}
                  </Header.Subheader>
                </Header.Content>
              </Header>
              <Divider />
              <EmployeeForm
                mode={this.props.route.mode}
                router={this.props.router}
              />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default EmployeeFormView
