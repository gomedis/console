// @flow

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Button} from 'semantic-ui-react/dist/es'
import _ from 'lodash'
import ReactJsonSchemaForm, {
  radioWidget,
  dobWidget,
  dropdownWidget
} from '../../components/ReactJsonSchemaForm/ReactJsonSchemaForm'
import UserSearchWidget from '../../components/ReactJsonSchemaForm/UserSearchWidget'
import AllBranchWidget from '../../components/ReactJsonSchemaForm/AllBranchWidget'
import type {EmployeeForm_viewer} from './__generated__/EmployeeForm_viewer.graphql'

import UpdateOrCreateEmployeeMutation from '../../mutations/UpdateOrCreateEmployeeMutation'

type State = {
  mode: 'edit' | 'create' | 'read',
  isMutating: boolean
}
type Props = {
  mode: 'edit' | 'create' | 'read',
  viewer?: ?EmployeeForm_viewer
}

export default class EmployeeFormComponent extends React.Component {
  defaultValue: {}
  props: Props
  state: State
  constructor(props: Props) {
    super(props)
    this.state = {
      mode: props.mode,
      isMutating: false
    }
  }

  handleFormSubmit = ({formData}: any) => {
    console.log('formdata submit', formData)
    this.setState({
      isMutating: true
    })
    const {id, branch, ...restInput} = formData
    console.log('restInput', restInput)
    const input = {
      clientMutationId: 'assasa',
      create: {
        ...restInput
      },
      update: {
        id: id ? id : '1',
        ...restInput
      }
    }

    UpdateOrCreateEmployeeMutation.commit(input, response => {
      this.setState({
        isMutating: false
      })
    })
  }

  render() {
    let formData
    if (this.props.viewer && this.props.viewer.Employee) {
      const employee = this.props.viewer.Employee
      const branchId = employee && employee.branch
        ? employee.branch.id
        : undefined
      const userId = employee && employee.user ? employee.user.id : undefined
      const {user, ...rest} = this.props.viewer.Employee
      formData = {
        ...rest,
        userId: userId,
        branchId: branchId
      }
    }
    console.log('formmode', this.props.mode)
    return (
      <div>
        <ReactJsonSchemaForm
          schema={employeeSchema(this.props)}
          uiSchema={employeeUiSchema(this.props)}
          liveValidate
          showErrorList
          onSubmit={this.handleFormSubmit}
          formData={formData}>
          <Button
            type="submit"
            size="large"
            positive
            disabled={this.state.isMutating}
            loading={this.state.isMutating}>
            Update
          </Button>
        </ReactJsonSchemaForm>
      </div>
    )
  }
}

const employeeSchema = props => {
  return {
    title: 'Employee Form',
    type: 'object',
    required: ['userId', 'fullName', 'dob', 'gender', 'salary', 'branchId'],
    properties: {
      // id: {
      //   type: 'string',
      //   title: 'Employee ID'
      // },
      userId: {
        type: 'string',
        title: 'User Account'
      },
      fullName: {
        type: 'string',
        title: 'Full Name',
        pattern: "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$"
      },
      gender: {
        title: 'Gender',
        type: 'string',
        enum: ['T1', 'T2'],
        enumNames: ['Pria', 'Wanita']
      },
      dob: {type: 'string', title: 'Date of Birth'},
      salary: {title: 'Salary', type: 'number', minimum: 0},
      branchId: {
        type: 'string',
        title: 'Branch'
      }
    }
  }
}
const mainSchema = props => {
  return {
    title: 'Combined Form',
    type: 'object',
    properties: {
      //user: {...userSchema(props)},
      employee: {...employeeSchema(props)}
    }
  }
}
const mainUiSchema = props => {
  return {
    employee: {
      ...employeeUiSchema(props)
    }
  }
}
const employeeUiSchema = props => {
  const {viewer} = props
  return {
    userId: {
      'ui:placeholder': 'Select User',
      'ui:widget': UserSearchWidget,
      'ui:readOnly': _.get(viewer, 'Employee.user.id', null) !== null
    },
    fullName: {
      'ui:placeholder': 'Isi Nama Lengkap',
      'ui:readOnly': true
    },
    salary: {
      'ui:placeholder': 'Isi Salary'
    },
    dob: {
      'ui:placeholder': 'Isi Tanggal Lahir',
      'ui:widget': dobWidget
    },
    gender: {
      'ui:placeholder': 'Pilih Gender',
      'ui:widget': dropdownWidget,
      'ui:options': {
        inline: false
      }
    },
    branchId: {
      'ui:widget': AllBranchWidget,
      'ui:options': {
        name: _.get(props, 'viewer.Employee.branch.name')
      }
    }
  }
}

// const userSchema = props => {
//   return {
//     title: 'User Form',
//     type: 'object',
//     required: ['email', 'avatarUrl'],
//     properties: {
//       // id: {
//       //   type: 'string',
//       //   title: 'User ID'
//       // },
//       email: {
//         type: 'string',
//         title: 'Email',
//         format: 'email'
//       },
//       avatarUrl: {
//         type: 'string',
//         title: 'Avatar Url',
//         format: 'uri'
//       }
//     }
//   }
// }

//EmployeeForm.propTypes = {}
//EmployeeForm.defaultProps = {}
