 // @flow

import React, {Component} from 'react'
import {
  createFragmentContainer,
  createRefetchContainer,
  graphql,
  QueryRenderer
} from 'react-relay'
import EmployeeFormComponent from './EmployeeFormComponent'
export default createFragmentContainer(
  EmployeeFormComponent,
  graphql.experimental`fragment EmployeeForm_viewer on Viewer{
    id
    Employee(id: $id){
      id
      fullName
      dob
      gender
      salary
      user{id}
      branch{id name}
    }
  }
  `
)
