// @flow

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {
  Header,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Label,
  Modal
} from 'semantic-ui-react/dist/es'
import {createFragmentContainer, graphql, QueryRenderer} from 'react-relay'

import EmployeeForm from './EmployeeForm'
import EmployeeFormComponent from './EmployeeFormComponent'

type Props = {
  relay: any,
  employeeId: ?string,
  mode: 'read' | 'edit' | 'create'
}

type State = {
  open: boolean
}

export default class EmployeeModal extends React.Component {
  props: Props
  state: State
  constructor(props: Props) {
    super(props)

    this.state = {
      open: false
    }
  }
  _renderEditForm = (propsx: Props) => {
    const {relay, employeeId, ...rest} = propsx
    return (
      <QueryRenderer
        environment={relay.environment}
        query={//language=GraphQL
        graphql.experimental`
                query EmployeeModal_Query($id: ID!){
                  viewer{
                  id
                    ...EmployeeForm_viewer
                  }
                }
              `}
        variables={{
          id: employeeId
        }}
        render={({error, props}) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            return <EmployeeForm {...rest} {...props} mode="edit" />
          }
          return <Loader active inline="centered" />
        }}
      />
    )
  }
  _renderCreateForm = (props: Props) => {
    return <EmployeeFormComponent {...props} mode="create" />
  }

  render() {
    console.log('RENDER employeeModal mode:', this.props.mode)
    //if (!this.state.modalOpen) return null
    const {relay, employeeId, mode, ...rest} = this.props
    return (
      <Modal
        closeIcon="close"
        dimmer={'inverted'}
        defaultOpen
        //open={false}
        //closeOnEscape={false}
        //closeOnRootNodeClick={false}
        size="small"
        //trigger={this.props.trigger}

        //defaultOpen={false}
        // onClose={() => {
        //   this.setState({
        //     open: false
        //   })
        // }}
        {...rest}>
        <Modal.Content>
          {mode === 'create'
            ? this._renderCreateForm(this.props)
            : this._renderEditForm(this.props)}
        </Modal.Content>
      </Modal>
    )
  }
}
