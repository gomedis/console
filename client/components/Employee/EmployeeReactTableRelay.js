// @flow

import React from 'react'
import {withRouter} from 'react-router'
//import  LazilyLoad, {importLazy} from '../../components/LazilyLoad/LazilyLoad'
import {
  Label,
} from 'semantic-ui-react/dist/es'
import {
  createRefetchContainer,
  graphql,
} from 'react-relay'
import ReactTableRelay from '../../components/ReactTableRelay/ReactTableRelay'
import DeleteEmployeeMutation from '../../mutations/DeleteEmployeeMutation'
import CellDetailPopup from '../../components/Umum/CellDetailPopup'
import EmployeeModal from './EmployeeModal'

import type {
  EmployeeReactTableRelay_viewer
} from './__generated__/EmployeeReactTableRelay_viewer.graphql'
type State = {
  mode: 'create' | 'read' | 'edit',
  isModalOpen: boolean,
  employeeId: ?string
}
type Props = {
  viewer: EmployeeReactTableRelay_viewer,
  relay: any,
  variables: any
}
class EmployeeReactTableRelay extends React.Component {
  state: State
  props: Props
  constructor(props: Props) {
    super(props)

    this.state = {
      isModalOpen: false,
      employeeId: null,
      mode: 'read'
    }
  }

  _handleDeleteClicked = employeeIds => {
    console.log('delete Clicked', employeeIds, this.props.viewer.id)
    // employeeIds.forEach(employeeId => {
    //   DeleteEmployeeMutation.commit(employeeId)
    // })

    //this.props.relay.refetch();
  }

  onExpandClicked = id => {
    console.log('onExpand', id)
    this.setState({
      isModalOpen: true,
      mode: 'edit',
      employeeId: id
    })
  }

  _handleAddClicked = () =>{
    console.log('addclicked')
    this.setState({
      isModalOpen: true,
      employeeId: null,
      mode: 'create'
    })
  }

  onModalMounted = () => {
    //console.log('mounted')
  }

  renderModal = () => {
    if (!this.state.isModalOpen) return null
    console.log('employeereacttablerelay mode: ', this.state.mode)
    return (
      <EmployeeModal
        mode={this.state.mode}
        relay={this.props.relay}
        //open={this.state.isModalOpen}

        employeeId={this.state.employeeId}
        // onMount={() => {
        //   console.log('MODAAAL MOUNTED')
        // }}
        onUnmount={() => {
          //console.log('MODAL UN-MOUNTED')
          // this.forceUpdate()
          this.setState({
            isModalOpen: false,
            employeeId: null
          })
        }}
      />
    )
  }

  render() {
    //console.log('RENDER EmployeeReactTableRelay')
    const {allEmployees} = this.props.viewer
    const columns = [
      {
        Header: 'Employee name',
        id: 'fullName',
        filterable: true,
        accessor: 'node',
        width: 250,
        style: {
          padding: 0
        },
        Cell: props => {
          return (
            <CellDetailPopup
              goto={`/employee/${props.value.id}`}
              onExpandClick={()=>{
                this.onExpandClicked(props.value.id)
              }}
              hoverable>
              {props.value.fullName}
            </CellDetailPopup>
          )
        },
        relayFilter: value => {
          return {
            fullName_starts_with: value
          }
        }
      },
      {
        Header: 'email',
        id: 'email',
        filterable: true,
        sortable: false,
        accessor: 'node.user.email',
        Cell: props => {
          return props.value ? props.value : <Label content="Orphan" />
        },
        relayFilter: value => {
          return {
            user: {
              email_starts_with: value
            }
          }
        }
      },

      {
        Header: 'Branch',
        id: 'branch',
        filterable: true,
        accessor: 'node.branch.name',
        width: 100,
        relayFilter: value => {
          return {
            branch: {
              name_starts_with: value
            }
          }
        }
      }
    ]
    return (
      <div style={{height: '100%'}}>
        {this.renderModal()}
        <ReactTableRelay
          commitDelete={DeleteEmployeeMutation.commit}
          onAddClick={this._handleAddClicked}
          edgesType="Employee"
          variables={this.props.variables}
          nonPaginateCount={allEmployees.count}
          relay={this.props.relay}
          data={allEmployees.edges}
          columns={columns}
        />
      </div>
    )
  }
}
//language=GraphQL
const query = graphql.experimental`
  query EmployeeReactTableRelayQuery(
  $first: Int
  $cursor: String
  $orderBy: EmployeeOrderBy
  $filter: EmployeeFilter
  $skip: Int
  ) {
    viewer {
      id
      ...EmployeeReactTableRelay_viewer
    }
  }
`
export default createRefetchContainer(
  withRouter(EmployeeReactTableRelay),
  //language=GraphQL
  graphql.experimental`fragment EmployeeReactTableRelay_viewer on  Viewer
  {
   id

   allEmployees(
     first: $first
     after: $cursor
     orderBy: $orderBy
     filter: $filter
     skip: $skip
   )
   @connection(key: "EmployeeList_allEmployees", filters: ["first", "after", "orderBy", "filter", "skip"])
   {
     count
     edges{
       node{
         id
         fullName
         updatedAt
         branch{
           name
         }
         createdAt
         user{
           email
         }
       }
     }
    }
  }
  `,
  query
)
