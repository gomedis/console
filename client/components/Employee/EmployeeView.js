// @flow
import React from 'react'
import {graphql, QueryRenderer} from 'react-relay'
import {currentRelay} from '../../createRelayEnvironment'
import RelayQueryLoader from '../../components/Umum/RelayQueryLoader'
import EmployeeViewComponent from './EmployeeViewComponent'
//language=GraphQL
const query = graphql.experimental`
  query EmployeeViewQuery(
    $first: Int
    $cursor: String
    $orderBy: EmployeeOrderBy
    $filter: EmployeeFilter
    $skip: Int
   
    ) {
    viewer {
      ...EmployeeReactTableRelay_viewer

    }
  }
 `
const EmployeeView = (propsx: any) => {
  const variables = {
    first: 10,
    cursor: null,
    orderBy: null,
    filter: null,
    skip: null
  }
  return (
    <QueryRenderer
      environment={currentRelay.env}
      query={query}
      variables={variables}
      render={({error, props}) => {
        if (error) {
          console.error('BRO', error.source)
          return <RelayQueryLoader text="PERMISSION ERROR" />
        }
        if (props) {
          return (
            <EmployeeViewComponent
              variables={variables}
              viewer={props.viewer}
              {...propsx}
            />
          )
        }
        return <RelayQueryLoader />
      }}
    />
  )
}
export default EmployeeView
