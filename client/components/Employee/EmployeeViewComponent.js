/* @flow */

import React, {Component} from 'react'
import {withRouter} from 'react-router'

import {
  Header,
  Divider,
  Statistic,
  Grid,
  Button,
  Dimmer,
  Loader,
  Segment,
  Input,
  Icon,
  Popup,
  Progress,
  Checkbox,
  Label
} from 'semantic-ui-react/dist/es'

import DeleteEmployeeMutation from '../../mutations/DeleteEmployeeMutation'

import EmployeeReactTableRelay from '../../components/Employee/EmployeeReactTableRelay'

export default class EmployeeViewComponent extends React.Component {
  props: {
    relay: any,
    viewer: any
  }
  state: {}
  static defaultProps: {}
  constructor(props: any) {
    super(props)
  }

  _handleDeleteClicked = (employeeIds: Array<string>) => {
    console.log('delete Clicked', employeeIds, this.props.viewer.id)
    // employeeIds.forEach(employeeId => {
    //   DeleteEmployeeMutation.commit(employeeId)
    // })
  }

  render() {
    return (
      <div
        style={{
          padding: '1em',
          height: '100%'
        }}>
        <EmployeeReactTableRelay {...this.props} />
      </div>
    )
  }
}
