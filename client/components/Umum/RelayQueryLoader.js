// @flow
import React,{Component} from 'react'
import {Dimmer, Loader} from 'semantic-ui-react/dist/es'

type Props = {
  text?: any
}

export default class RelayQueryLoader extends React.Component {
  props: Props
  constructor(props: Props) {
    super(props)
  }
  render(){
    return (
      <Dimmer active inverted>
        <Loader inline inverted>
          {this.props.text? this.props.text :  'Loading'}
        </Loader>
      </Dimmer>
    )
  }
}

