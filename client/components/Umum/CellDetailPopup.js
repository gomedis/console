// @flow
import React,{Component} from 'react'
import {Link} from 'react-router'
import {Popup, Button} from 'semantic-ui-react/dist/es'

const CustomButton = props =>
  <Button
    as={props.onClick ? undefined : Link}
    size="mini"
    basic
    primary
    circular
    //inverted
    //color="red"
    //basic
    {...props}
  />

function ModalButtonWrapper(props) {
  return props.modalButton
}

type State = {
  isOpen: boolean
}

type Props = {
  goto: string,
  children?: any,
  onExpandClick?: any,
  ModalTrigger?: any,
  TheModal?: any
}
class CellDetailPopup extends React.Component {
  state: State
  props: Props
  constructor(props: Props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  handleOpen = () => {
    this.setState({isOpen: true})
  }

  handleClose = () => {
    this.setState({isOpen: false})
  }

  handleExpandClick = (e: SyntheticInputEvent) => {
    this.handleClose()
    this.props.onExpandClick && this.props.onExpandClick(e)
  }

  render() {
    const {onExpandClick, TheModal, ModalTrigger, goto, ...rest} = this.props
    return (
      <Popup
        //inverted

        hideOnScroll
        size="mini"
        open={this.state.isOpen}
        onClose={this.handleClose}
        onOpen={this.handleOpen}
        basic
        style={{
          background: 'transparent',
          boxShadow: 'none',
          padding: 0,
          border: 'none'
        }}
        flowing
        className="ui transition"
        trigger={
          <div
            style={{
              height: '100%',
              //background: 'cornsilk'
              padding: '7px 5px'
            }}>
            {this.props.children}
          </div>
        }
        offset={-110}
        position="right center"
        hoverable
        {...rest}>
        <div>
          {this.props.onExpandClick
            ? <CustomButton icon="expand" onClick={this.handleExpandClick} />
            : null}
          {/*<CustomButton icon="expand" to={`${goto}`} />*/}
          <CustomButton icon="edit" to={`${goto}/edit`} />
          <CustomButton icon="trash" to={`${goto}/delete`} />
        </div>
      </Popup>
    )
  }
}

export default CellDetailPopup
