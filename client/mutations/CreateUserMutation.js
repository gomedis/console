// @flow

import {graphql} from 'react-relay'
import {commitMutationPromise} from './mutationHelper'
import {currentRelay} from '../createRelayEnvironment'
import type {
  SignupUserInput,
  SignupUserInput_employee,
  SignupUserInput_authProvider_auth0,
  CreateUserMutationResponse,
  CreateUserMutationResponse_user,
} from './__generated__/CreateUserMutation.graphql'
import type {Environment} from 'react-relay'

const mutation = graphql.experimental`
  mutation CreateUserMutation($input: SignupUserInput!){
    createUser(input: $input){
      user{
        id
      }
    }
  }
`
function commit(
  input: SignupUserInput
): Promise<CreateUserMutationResponse_user> {
  
  return commitMutationPromise(currentRelay.env, {
    mutation,
    variables: {
      input: {
        clientMutationId: 'abcd',
        ...input
      }
    }
  })
}
// function commit(
//   environment: any,
//   input: SignupUserInput
// ) {
//   return commitMutation(environment, {
//     mutation,
//     variables: {
//       input: {
//         clientMutationId: 'abcd',
//         authProvider: {
//           auth0: {
//             idToken: authResult.idToken
//           }
//         },
//         email: userProfile.email ? userProfile.email : '',
//         avatarUrl: userProfile.picture
//       }
//     },
//   })
// }

export default {commit}
