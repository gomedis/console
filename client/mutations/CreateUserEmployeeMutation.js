// @flow

import {graphql} from 'react-relay'
import {commitMutationPromise} from './mutationHelper'

import type {
  SignupUserInput,
  CreateUserEmployeeMutationResponse
} from './__generated__/CreateUserEmployeeMutation.graphql'

import type {Environment} from 'react-relay'

const mutation = graphql.experimental`
  mutation CreateUserEmployeeMutation($input: SignupUserInput!){
    createUser(input: $input){
      user{
        id
        email
        employee{
          id
          fullName
        }
      }
    }
  }
`
function commit(input: SignupUserInput, 
callback: (x: CreateUserEmployeeMutationResponse) => void
){
  
}
