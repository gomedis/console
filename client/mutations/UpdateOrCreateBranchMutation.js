// @flow

import {graphql, commitMutation} from 'react-relay'
import {commitMutationPromise} from './mutationHelper'
import {ConnectionHandler} from 'relay-runtime'
import {currentRelay} from '../createRelayEnvironment'
import dumper from 'relay-runtime/lib/RelayStoreProxyDebugger'
import type {UpdateOrCreateBranchMutationVariables} from './__generated__/UpdateOrCreateBranchMutation.graphql'
import type {Environment} from 'react-relay'
let tempID = 0
const mutation = graphql.experimental`
  mutation UpdateOrCreateBranchMutation($input: UpdateOrCreateBranchInput!){
    updateOrCreateBranch(input: $input){
      edge{
        node{
          ...BranchListMenuCard_branch
        }
      }
      branch{
        id
        name
        updatedAt
        location{lat lng}
      }
    }
  }
`
type mutationConfig = {}
function sharedUpdater(store, newEdge) {
  const viewerProxy = store.get('viewer-fixed')
  const conn = ConnectionHandler.getConnection(
    viewerProxy,
    'BranchList_allBranches',
    {first: 100}
  )
  ConnectionHandler.insertEdgeAfter(conn, newEdge)
  // if (conn) {
    
  // }
  dumper.dump(store)
}
function commit(variables: UpdateOrCreateBranchMutationVariables) {
  const {input} = variables
  const {update} = input
  if (update.id !== 'fake'){
    console.log('update!!!!!')
    return commitMutation(currentRelay.env,{
      mutation,
      variables,
      optimisticResponse: {
        updateOrCreateBranch:{
          branch: {
            city:"tai",
            createdAt: "pussy",
            updatedAt: "pussy",
            id: update.id,
            name: "myname"
          }
        }
      }
    })
  }
  return commitMutation(currentRelay.env, {
    mutation,
    variables,
    updater: store => {
      const payload = store.getRootField('updateOrCreateBranch')
      console.log('payload',payload)
      const newEdge = payload.getLinkedRecord('edge')
      console.log('newEdge',newEdge)
      sharedUpdater(store, newEdge)
    },
    optimisticUpdater: store => {
      console.log('optimisticUpdater')
      const id = 'client:newBranch:' + tempID++
      const node = store.create(id, 'Branch')

      const employees = store.create(id + ':employees', 'EmployeeConnection')
      employees.setValue(0, 'count')
      node.setValue(id, 'id')
      node.setValue(input.create.name, 'name')
      node.setValue(input.create.city, 'city')
      
      node.setLinkedRecord(employees, 'employees')
      const newEdge = store.create('client:newEdge:' + tempID++, 'BranchEdge')
      newEdge.setLinkedRecord(node, 'node')
      sharedUpdater(store, newEdge)
    }
  })
}

export default {commit}
