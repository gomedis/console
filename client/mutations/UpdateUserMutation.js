// @flow

import {graphql} from 'react-relay'
import {commitMutationPromise} from './mutationHelper'
const mutation = graphql.experimental`
  mutation UpdateUserMutation($input: UpdateUserInput!){
    updateUser(input: $input){
      user{
        id
        employee{
          id
        }
      }
      
    }
  }
`
function commit(
  environment: any,
  userProfile: any,
  onError: any,
  onSuccess: any
) {
  console.log(userProfile, 'profile')
  commitMutationPromise(environment, {
    mutation,
    variables: {
      input: {
        clientMutationId: 'abcd',
        ...userProfile
      }
    },
    onError: onError,
    onCompleted: onSuccess
  })
}

export default {commit}
