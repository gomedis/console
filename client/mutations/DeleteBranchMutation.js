// @flow

import {graphql} from 'react-relay'
import {ConnectionHandler} from 'relay-runtime'
import {commitMutationPromise} from './mutationHelper'
import {currentRelay} from '../createRelayEnvironment'
import dumper from 'relay-runtime/lib/RelayStoreProxyDebugger'
import type{
  DeleteBranchMutationVariables
} from './__generated__/DeleteBranchMutation.graphql'
const mutation = graphql.experimental`
  mutation DeleteBranchMutation($input: DeleteBranchInput!){
    deleteBranch(input: $input){
      branch{
        id
      }
    }
  }
`
function sharedUpdater(store, branchId){
  const viewerProxy = store.get('viewer-fixed')
  const conn = ConnectionHandler.getConnection(
    viewerProxy,
    'BranchList_allBranches',
    {first: 100}
  )
  ConnectionHandler.deleteNode(conn, branchId)
  dumper.dump(store)
}

function commit(id: string ){
  const variables: DeleteBranchMutationVariables =  {
    input:{
      clientMutationId:"ssss",
      id: id
    }
  }
  return commitMutationPromise(currentRelay.env, {
    mutation,
    variables,
    updater: store =>{
      sharedUpdater(store, id)
    },
    optimisticUpdater: store =>{
      sharedUpdater(store, id)
    }
  })
}

export default {commit}
