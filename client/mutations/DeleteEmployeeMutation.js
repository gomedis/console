// @flow

import {graphql} from 'react-relay'
import {ConnectionHandler} from 'relay-runtime'
import {commitMutationPromise} from './mutationHelper'
import type {Environment} from 'react-relay'
import dumper from 'relay-runtime/lib/RelayStoreProxyDebugger'
import {currentRelay} from '../createRelayEnvironment'
const mutation = graphql.experimental`
  mutation DeleteEmployeeMutation($input: DeleteEmployeeInput!){
      deleteEmployee(input: $input){
        deletedId
        viewer{
          allUsers{
            count
          }
          allBranches{edges{node{
            employees{
                count
              }}
            }
          }
        }
      }
  }
`
function commit(employeeId: string, connFilter: any) {
  const connectionName = 'EmployeeList_allEmployees'
  console.log('commit data', employeeId)
  return commitMutationPromise(currentRelay.env, {
    mutation,
    variables: {
      input: {
        id: employeeId,
        clientMutationId: 'abcd'
      }
    },
    updater: store => {
      const payload = store.getRootField('deleteEmployee')
      sharedDelete(
        store,
        connectionName,
        payload.getValue('deletedId'),
        connFilter
      )
    }
    // optimisticUpdater: store => {
    //   dumper.dump(store)
    //   sharedDelete(store, 'EmployeeListX_allEmployees', employeeId)
    // }
  })
}

export function sharedDelete(
  store: any,
  connectionName: string,
  deletedID: string,
  connFilter: any
) {
  dumper.dump(store)
  const viewerProxy = store.get('viewer-fixed')
  console.log('connFilter', connFilter)
  const conn = ConnectionHandler.getConnection(
    viewerProxy,
    connectionName,
    connFilter
  )
  console.log('CONN', conn)
  if (conn) {
    conn.setValue(conn.getValue('count') - 1, 'count')
    ConnectionHandler.deleteNode(conn, deletedID)
  }
  // deletedIDs.forEach((deletedID) =>
  //   ConnectionHandler.deleteNode(conn, deletedID)
  // );
}

// function sharedUpdater(store, viewerId, deletedID) {
//   const viewerProxy = store.get(viewerId)

//   const conn = ConnectionHandler.getConnection(
//     viewerProxy,
//     'EmployeeList_allEmployees'
//   )

//   //dumper.dump(store)

//   ConnectionHandler.deleteNode(conn, deletedID)

//   // deletedIDs.forEach((deletedID) =>
//   //   ConnectionHandler.deleteNode(conn, deletedID)
//   // );
// }
export default {commit}
