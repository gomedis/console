// @flow

import {graphql} from 'react-relay'
import {commitMutationPromise} from './mutationHelper'

const mutation = graphql.experimental`
  mutation UpdateUserEmployeeMutation(
  $employee: UpdateEmployeeInput!
  $user: UpdateUserInput!
  ){
    updateEmployee(input: $employee){
      user{
        id
      }

    }
    updateUser(input: $user){
      employee{
        id
      }
    }
  }
`


function commit(
  environment: any,
  input: any,
  onError: any,
  onSuccess: any
) {
  console.log(input, 'input profile')
  commitMutationPromise(environment, {
    mutation,
    variables: {
      employee: {
        clientMutationId: 'abcdx',
        ...input.employee
      },
      user: {
        clientMutationId: 'abcd',
        ...input.user
      }
    },
    onError: onError,
    onCompleted: onSuccess
  })
}

export default {commit}
