// @flow

import {graphql, commitMutation} from 'react-relay'
import {currentRelay} from '../createRelayEnvironment'
import type {UpdateBranchMutationVariables} from './__generated__/UpdateBranchMutation.graphql'

const mutation = graphql.experimental`
  mutation UpdateBranchMutation($input: UpdateBranchInput!){
    updateBranch(input: $input){
      branch{
        # need fragment spread of branch form/detail
        id
        name
        city
        updatedAt
        location{lat lng}
      }
    }
  }
`
function commit(variables: UpdateBranchMutationVariables) {
  const {input} = variables
  return commitMutation(currentRelay.env,{
    mutation,
    variables,
    optimisticResponse: {
      updateBranch:{
        branch: input
      }
    }
  })
}

export default {commit}
