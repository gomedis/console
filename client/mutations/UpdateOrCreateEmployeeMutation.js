// @flow

import {graphql, commitMutation} from 'react-relay'
import {commitMutationPromise} from './mutationHelper'
import {currentRelay} from '../createRelayEnvironment'
import type {
  UpdateOrCreateEmployeeInput,
  UpdateOrCreateEmployeeMutationResponse
} from './__generated__/UpdateOrCreateEmployeeMutation.graphql'

const mutation = graphql.experimental`
  mutation UpdateOrCreateEmployeeMutation(
    $input: UpdateOrCreateEmployeeInput!
  ){
    updateOrCreateEmployee(input: $input){
      employee{
        id
        fullName
        dob
        gender
        salary
        branch{id}
      }
    }
  }
`
function commit(
  input: UpdateOrCreateEmployeeInput,
  callback: (x: UpdateOrCreateEmployeeMutationResponse) => void
) {
  const {update} = input
  const {branchId, ...rest} = update
  
  commitMutation(currentRelay.env, {
    mutation,
    variables: {
      input
    },
    optimisticResponse: {
      updateOrCreateEmployee: {
        employee: {
          ...rest,
          branch: {id: branchId}
        }    
      }
    },
    onCompleted: response => {
      callback(response)
    }
  })
}

export default {commit}
