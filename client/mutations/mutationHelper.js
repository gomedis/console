// @flow

import {ConnectionHandler} from 'relay-runtime'
import {commitMutation} from 'react-relay'
import type {Environment} from 'react-relay'

type PayloadError = {
  message: string,
  locations?: Array<{
    line: number,
    column: number
  }>
}

export function sharedDelete(
  store: any,
  connectionName: string,
  viewerId: string,
  deletedID: string
) {
  //dumper.dump(store)
  const viewerProxy = store.get(viewerId)
  console.log('VIEWERPROXY', viewerProxy)
  const conn = ConnectionHandler.getConnection(viewerProxy, connectionName)
  console.log('CONN', conn)
  conn.setValue(conn.getValue('count') - 1, 'count')

  ConnectionHandler.deleteNode(conn, deletedID)

  // deletedIDs.forEach((deletedID) =>
  //   ConnectionHandler.deleteNode(conn, deletedID)
  // );
}

export function commitMutationPromise(environment: Environment, options: any) {
  return new Promise((resolve, reject) => {
    commitMutation(environment, {
      ...options,

      onError: (errors) => {
        console.error(errors)
        reject(errors)
      },
      onCompleted: response => {
        resolve(response)
      }
    })
  })
}

export function sharedUpdate(
  store: any,
  connectionName: string,
  updatedId: string
): void {}
