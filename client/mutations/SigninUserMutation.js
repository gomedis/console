// @flow
import {graphql} from 'react-relay'
import {commitMutationPromise} from './mutationHelper'


const mutation = graphql.experimental`
  mutation SigninUserMutation($input: SigninUserInput!){
    signinUser(input: $input){
      token
      user{
        id
      }
    }
  }
`

function commit(environment: any, authResult: any) {
  return commitMutationPromise(environment, {
    mutation,
    variables: {
      input: {
        auth0: {
          idToken: authResult.idToken
        },
        clientMutationId: 'abcd'
      }
    }
  })
}

export default {commit}
