// @flow

import {graphql, commitMutation} from 'react-relay'
import {ConnectionHandler} from 'relay-runtime'
import {currentRelay} from '../createRelayEnvironment'
import dumper from 'relay-runtime/lib/RelayStoreProxyDebugger'
import type {CreateBranchMutationVariables} from './__generated__/CreateBranchMutation.graphql'
let tempID = 0
const mutation = graphql.experimental`
  mutation CreateBranchMutation($input: CreateBranchInput!){
    createBranch(input: $input){
      edge{
        node{
          ...BranchListMenuCard_branch
        }
      }
    }
  }
`
function sharedUpdater(store, newEdge) {
  const viewerProxy = store.get('viewer-fixed')
  const conn = ConnectionHandler.getConnection(
    viewerProxy,
    'BranchList_allBranches',
    {first: 100}
  )
  ConnectionHandler.insertEdgeAfter(conn, newEdge)
  // if (conn) {
    
  // }
  dumper.dump(store)
}
function commit(variables: CreateBranchMutationVariables) {
  const {input} = variables

  return commitMutation(currentRelay.env, {
    mutation,
    variables,
    updater: store => {
      const payload = store.getRootField('createBranch')
      console.log('payload',payload)
      const newEdge = payload.getLinkedRecord('edge')
      console.log('newEdge',newEdge)
      sharedUpdater(store, newEdge)
    },
    optimisticUpdater: store => {
      console.log('optimisticUpdater')
      const id = 'client:newBranch:' + tempID++
      const node = store.create(id, 'Branch')

      const employees = store.create(id + ':employees', 'EmployeeConnection')
      employees.setValue(0, 'count')
      node.setValue(id, 'id')
      node.setValue(input.name, 'name')
      node.setValue(input.city, 'city')
      
      node.setLinkedRecord(employees, 'employees')
      const newEdge = store.create('client:newEdge:' + tempID++, 'BranchEdge')
      newEdge.setLinkedRecord(node, 'node')
      sharedUpdater(store, newEdge)
    }
  })
}

export default {commit}
