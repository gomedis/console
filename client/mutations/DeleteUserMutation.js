// @flow

import {graphql} from 'react-relay'
import {ConnectionHandler} from 'relay-runtime'
import dumper from 'relay-runtime/lib/RelayStoreProxyDebugger'
import {commitMutationPromise} from './mutationHelper'
import {currentRelay} from '../createRelayEnvironment'


const mutation = graphql.experimental`
  mutation DeleteUserMutation($input: DeleteUserInput!){
    deleteUser(input: $input){
      deletedId
      viewer{
          allUsers{
            count
          }
      }
    }
  }

`
function sharedUpdater(store, viewerId, deletedID) {
  //dumper.dump(store)
  const viewerProxy = store.get(viewerId)

  const conn = ConnectionHandler.getConnection(viewerProxy, 'UserList_allUsers')

  conn.setValue(conn.getValue('count') - 1, 'count')

  ConnectionHandler.deleteNode(conn, deletedID)

  // deletedIDs.forEach((deletedID) =>
  //   ConnectionHandler.deleteNode(conn, deletedID)
  // );
}

export function sharedDelete(
  store: any,
  connectionName: string,
  deletedID: string
) {
  dumper.dump(store)
  const viewerProxy = store.get('viewer-fixed')
  console.log('VIEWERPROXY', viewerProxy)
  const usersConn = ConnectionHandler.getConnection(viewerProxy, connectionName)

  console.log('usersConn', usersConn)

  if (usersConn) {
    usersConn.setValue(usersConn.getValue('count') - 1, 'count')
    ConnectionHandler.deleteNode(usersConn, deletedID)
  }
}

function commit(userId: string) {
  return commitMutationPromise(currentRelay.env, {
    mutation,
    variables: {
      input: {
        id: userId,
        clientMutationId: 'abcd'
      }
    },
    //
    // updater: store => {
    //   const payload = store.getRootField('deleteUser')
    //   //dumper.dump(store)
    //   //const errorPayload = store.getRootField('errors')
    //   //console.log(errorPayload.getValue('message'))
    //   if (payload === null) {
    //     return
    //   }
    //
    //   const deletedId = payload.getValue('deletedId')
    //   sharedDelete(store, 'UserList_allUsers', viewerID, deletedId)
    // },
    // optimisticUpdater: store => {
    //   sharedDelete(store, 'UserList_allUsers', viewerID, userId)
    //   //dumper.dump(store)
    // },
  })
}

export default {commit}
