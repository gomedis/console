// @flow

import {graphql} from 'react-relay'
import dumper from 'relay-runtime/lib/RelayStoreProxyDebugger'
import {commitMutationPromise, sharedUpdate} from './mutationHelper'

import type {Environment} from 'react-relay'
import type UpdateEmployeeInput from './__generated__/UpdateEmployeeMutation.graphql'

const mutation = graphql.experimental`
  mutation UpdateEmployeeMutation(
    $employee: UpdateEmployeeInput!
  ){
    updateEmployee(input: $employee){
      employee{
        id
        fullName
        dob
        gender
        salary
        user{id email avatarUrl}
        branch{
          id
          employees{
            count
          }
        }
        
      }
      edge{
        node{
          id
        }
      }
      viewer{
        allBranches{
          edges{
            node{
              employees{
                count
              }
            }
          }
        }
      }
    
    }
  }
`

function commit(environment: Environment, input: UpdateEmployeeInput, onError: any, onSuccess: any) {
  console.log(input, 'employee variabel')
  const {user, branch, ...employee} = input
  
  
  commitMutationPromise(environment, {
    mutation,
    variables: {
      employee: {
        clientMutationId: 'abcdx',
        ...employee,
        branchId: branch.id
      }
    },
    // updater: store => {
    //   dumper.dump(store)
    // },
    optimisticResponse: () => {
      
      return {
        updateEmployee: {
          employee: {
            ...input
          }
        }
      }
    },
    // optimisticUpdater: store => {
    //   //sharedDelete(store, 'UserList_allUsers', viewerID, userId)
    //   dumper.dump(store)
    //
    // },
    onError: onError,
    onCompleted: onSuccess
  })
}

export default {commit}
