// @flow

import React from 'react'
import ReactDOM from 'react-dom'

//import 'semantic-ui-css/semantic.css';

import { AppContainer as ReactHotContainer } from 'react-hot-loader'

import Root from './root'


const render = Component => {
  ReactDOM.render(
    <ReactHotContainer>
      <Component />
    </ReactHotContainer>,
    document.getElementById('app')
  )
}

render(Root)

if (module.hot) {
  // $FlowFixMe
  module.hot.accept('./root', () => {
    render(Root)
  })
}
