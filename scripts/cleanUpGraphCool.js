// @flow
require('dotenv').config()

import Faker from 'faker/locale/id_ID'
// $FlowFixMe
import request from 'request'
const _ = require('lodash')

const Api = process.env.GRAPHCOOL_URL
const superToken = process.env.GRAPHCOOL_SUPER_TOKEN

// const ManagementClient = require('auth0').ManagementClient;
//
// const management = new ManagementClient({
//   token: process.env.AUTH0_SEED_SUPER_TOKEN,
//   domain: process.env.AUTH0_DOMAIN
// });

const mgmtAuth = require('./auth0Helper').mgmtAuth

function sendrequest(data) {
  request(
    {
      url: Api,
      method: 'POST',
      json: true,
      headers: {
        'content-type': 'application/json',
        // $FlowFixMe
        authorization: `Bearer ${superToken}`
      },
      body: data
    },
    (error, response, body) => {
      if (!error && response.statusCode === 200) {
        console.log(JSON.stringify(body, null, 2))
      } else {
        console.log(error)
        console.log(response.statusCode)
      }
    }
  )
}
function headers(body) {
  return {
    url: Api,
    method: 'POST',
    json: true,
    headers: {
      'content-type': 'application/json',
      // $FlowFixMe
      authorization: `Bearer ${superToken}`
    },
    body: body
  }
}

const getUsers = () => {
  const usersQuery = {
    query: `
    query getallUsers{
      viewer {
        allUsers(first: 2000) {
          edges {
            node {
              id
              username
            }
          }
        }
      }
    }
  `
  }
  const header = headers(usersQuery)
  request(header, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      const edges = body.data.viewer.allUsers.edges
      //console.log(JSON.stringify(edges, null, 2));
      edges.forEach(deleteUser)
      //createpatient(userId)
    } else {
      console.log(
        'createUSerFailed: ',
        'error:',
        error,
        'statuscode:',
        response.statusCode
      )
      console.log('body: ', body)
    }
  })
}

function deleteUser(element, index, array) {
  const id = element.node.id
  console.log(id)

  const data = {
    query: `
    mutation DeleteUser($user: DeleteUserInput!) {
      deleteUser(input: $user) {
        changedUser {
          id
          username
          employee{
            id
          }
          patient{
            edges{
              node{
                id
              }
            }
          }
        }
      }
    }
  `,
    variables: {
      user: {
        id: id
      }
    }
  }

  const header = headers(data)
  request(header, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      const edges = body.data.deleteUser.changedUser.patient.edges
      const employee = body.data.deleteUser.changedUser.employee

      edges.forEach(element => {
        deleteNode(element.node.id)
      })

      if (employee) {
        deleteNode(employee.id)
      }
    } else {
      console.log(
        'createUSerFailed: ',
        'error:',
        error,
        'statuscode:',
        response.statusCode
      )
      console.log('body: ', body)
    }
  })
}

function deleteType(typeName) {
  const typeQuery = {
    //language=GraphQL
    query: `
    query getData{
      viewer {
        ${typeName}(first: 500) {
          edges {
            node {
            __typename
            id
              
            }
          }
        }
      }
    }
  `
  }
  const header = headers(typeQuery)
  request(header, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      const edges = _.get(body, `data.viewer.${typeName}.edges`)
      edges.forEach(deleteNode)
    } else {
      console.log(
        'createUSerFailed: ',
        'error:',
        error,
        'statuscode:',
        response.statusCode
      )
      console.log('body: ', body)
    }
  })
}

function deleteNode(element, index, array) {
  const nodeType = element.node.__typename

  const data = {
    query: `
    mutation Delete${nodeType}($input: Delete${nodeType}Input!) {
      delete${nodeType}(input: $input) {
        deletedId
      }
    }
  `,
    variables: {
      input: {
        id: element.node.id,
        clientMutationId: 'aaa'
      }
    }
  }

  const header = headers(data)
  request(header, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      console.log('Deleted ' + nodeType, element.node.id)
    } else {
      console.log(data)
      console.log(
        'delete node failed: ',
        'error:',
        error,
        'statuscode:',
        response.statusCode
      )
      console.log('body: ', body)
    }
  })
}
function deleteAllAuth0() {
  mgmtAuth(mgmt => {
    mgmt.management.users.getAll(
      {
        per_page: 100,
        fields: 'user_id'
      },
      (err, users) => {
        if (err) {
          console.log(err)
        }
        if (!users) return

        console.log(JSON.stringify(users, null, 2))
        return Promise.all(
          users.map(user => {
            return mgmt.management.users.delete({id: user.user_id}, err => {
              console.log('ALL DELETED')
            })
          })
        )
      }
    )
  })
}

//getUsers()
deleteType('allUsers')
deleteType('allPatients')
deleteType('allCustomers')
deleteType('allEmployees')
deleteType('allBranches')
deleteType('allExams')
deleteType('allBranchExams')
deleteType('allOrders')

deleteAllAuth0()
