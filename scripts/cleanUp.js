// @flow weak
require("dotenv").config();

import Faker from "faker/locale/id_ID";
// $FlowFixMe
import request from "request";
const _ = require("lodash");

const scapholdApi = process.env.GRAPHCOOL_URL;
// $FlowFixMe
const superToken: string = process.env.GRAPHCOOL_SUPER_TOKEN;




function sendrequest(data) {
  request(
    {
      url: scapholdApi,
      method: "POST",
      json: true,
      headers: {
        "content-type": "application/json",
        authorization: `Bearer ${superToken}`
      },
      body: data
    },
    (error, response, body) => {
      if (!error && response.statusCode == 200) {
        console.log(JSON.stringify(body, null, 2));
      } else {
        console.log(error);
        console.log(response.statusCode);
      }
    }
  );
}
function headers(body) {
  return {
    url: scapholdApi,
    method: "POST",
    json: true,
    headers: {
      "content-type": "application/json",
      authorization: `Bearer ${superToken}`
    },
    body: body
  }
}


const getUsers = () => {

  const usersQuery = {
    query: `
    query getallUsers{
      viewer {
        allUsers(first: 2000) {
          edges {
            node {
              id
              username
            }
          }
        }
      }
    }
  `
  };
  const header = headers(usersQuery)
  request(header,
    (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const edges = body.data.viewer.allUsers.edges
        //console.log(JSON.stringify(edges, null, 2));
        edges.forEach(deleteUser)
        //createpatient(userId)
      } else {
        console.log('createUSerFailed: ', 'error:', error, 'statuscode:', response.statusCode);
        console.log('body: ', body);
      }
    }
  );

}

function deleteUser(element, index, array){
  const id = element.node.id
  console.log(id)

  const data = {
    query: `
    mutation DeleteUser($user: DeleteUserInput!) {
      deleteUser(input: $user) {
        changedUser {
          id
          username
          employee{
            id
          }
          patient{
            edges{
              node{
                id
              }
            }
          }
        }
      }
    }
  `,
    variables: {
      "user": {
        "id": id
      }
    }
  };

  const header = headers(data)
  request(header,
    (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const edges = body.data.deleteUser.changedUser.patient.edges
        const employee = body.data.deleteUser.changedUser.employee

        edges.forEach((element) => {
          deleteNode(element.node.id)
        })

        if (employee){
          deleteNode(employee.id)
        }

      } else {
        console.log('createUSerFailed: ', 'error:', error, 'statuscode:', response.statusCode);
        console.log('body: ', body);
      }
    }
  );
}

function deleteType(typeName){
  
  const typeQuery = {
    query: `
    query getData{
      viewer {
        ${typeName}(first: 2000) {
          edges {
            node {
              id
            }
          }
        }
      }
    }
  `
  };
  const header = headers(typeQuery)
  request(header,
    (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const edges = _.get(body, `data.viewer.${typeName}.edges`)
        //console.log(JSON.stringify(edges, null, 2));
        edges.forEach(deleteNode)
        //createpatient(userId)
      } else {
        console.log('createUSerFailed: ', 'error:', error, 'statuscode:', response.statusCode);
        console.log('body: ', body);
      }
    }
  );

}

function deleteNode(element, index, array){
  const id = element.node.id
  const decoded = new Buffer(id, 'base64').toString()
  const nodeType = decoded.split(':').shift()

  const data = {
    query: `
    mutation Delete${nodeType}($input: Delete${nodeType}Input!) {
      delete${nodeType}(input: $input) {
        changed${nodeType} {
          id
         
        }
      }
    }
  `,
    variables: {
      input: {
        "id": id
      }
    }
  }

  const header = headers(data)
  request(header,
    (error, response, body) => {
      if (!error && response.statusCode == 200) {
        console.log('Deleted ' + nodeType, id)
      } else {
        console.log(data)
        console.log('delete node failed: ', 'error:', error, 'statuscode:', response.statusCode);
        console.log('body: ', body);
      }
    }
  );
}

//getUsers()
deleteType('allUsers')
deleteType('allPatients')
deleteType('allCustomers')
deleteType('allEmployees')
deleteType('allBranches')
deleteType('allExams')
deleteType('allBranchExams')
deleteType('allOrders')

