const fetch = require('node-fetch');
const fs = require('fs');
const {
  buildClientSchema,
  introspectionQuery,
  printSchema,
} = require('graphql/utilities');

const path = require('path');
const schemaPath = path.join(__dirname, '../server/data/scapholdschema');
const graphQLServer = 'https://us-west-2.api.scaphold.io/graphql/medic-lab';

console.log('Oke Oce')

// fetch(`${graphQLServer}`, {
//   method: 'POST',
//   headers: {
//     'Accept': 'application/json',
//     'Content-Type': 'application/json'
//   },
//   body: JSON.stringify({'query': introspectionQuery}),
// }).then(res => res.json()).then(schemaJSON => {
//   fs.writeFileSync(
//     `${schemaPath}.json`,
//     JSON.stringify(schemaJSON, null, 2)
//   );
//
//   // Save user readable type system shorthand of schema
//   const graphQLSchema = buildClientSchema(schemaJSON.data);
//   fs.writeFileSync(
//     `${schemaPath}.graphql`,
//     printSchema(graphQLSchema)
//   );
//
//   //process.exit()
// });

const schemaPath2 = path.join(__dirname, '../server/data/graphcoolschema');
const graphQLServer2 = 'https://api.graph.cool/relay/v1/cj1dh6pltkxcs0198jwyddjpr';

fetch(`${graphQLServer2}`, {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({'query': introspectionQuery}),
}).then(res => res.json()).then(schemaJSON => {
  fs.writeFileSync(
    `${schemaPath2}.json`,
    JSON.stringify(schemaJSON, null, 2)
  );
  
  // Save user readable type system shorthand of schema
  const graphQLSchema = buildClientSchema(schemaJSON.data);
  fs.writeFileSync(
    `${schemaPath2}.graphql`,
    printSchema(graphQLSchema)
  );
  
  //process.exit()
});