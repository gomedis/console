// @flow weak
require("dotenv").config()
// $FlowFixMe
const request = require('request')
const Faker = require('faker/locale/id_ID')
//import Faker from "faker/locale/id_ID"
// $FlowFixMe
const chance = require("chance").Chance()
const _ = require("lodash")

const apiUrl = process.env.GRAPHCOOL_URL
const supertoken: any = process.env.GRAPHCOOL_SUPER_TOKEN

function headers(body) {
  return {
    url: apiUrl,
    method: "POST",
    json: true,
    headers: {
      "content-type": "application/json",
      authorization: `Bearer ${supertoken}`
    },
    body: body
  }
}

function sendrequest(data) {
  request(headers(data), (error, response, body) => {
    if (!error && response.statusCode === 200) {
      console.log(JSON.stringify(body, null, 2))
    } else {
      console.log(error)
      console.log(response.statusCode)
    }
  })
}

function makeUser(callback) {
  const newUser = {
    query: `
    mutation CreateUser($user: SignupUserInput!) {
      createUser(input: $user) {
        user {
          id
        }
      }
    }
  `,
    variables: {
      "user": {
        "clientMutationId": "sssd",
        "authProvider": {
          "email": {
            "email": Faker.internet.email().toLowerCase(),
            "password": "1234"
          }
        }
      }
    }
  }
  const header = headers(newUser)
  
  request(header, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      const userId = body.data.createUser.user.id
      
      callback(userId)
    } else {
      console.log(
        "createUserFailed: ",
        "error:",
        error,
        "statuscode:",
        response.statusCode
      )
      console.log("body: ", body)
    }
  })
}
function genGender (){
  return chance.gender() === 'Female' ? 'T2' : 'T1'
}


function createPatient(howMuch) {
  createCustomer(customerId => {
    _.times(howMuch, i => {
      const data = {
        query: `
        mutation CreatePatient($input: CreatePatientInput!) {
          createPatient(input: $input) {
            patient{
              id
              customer{
                id
              }
            }
          }
        }
      `,
        variables: {
          input: {
            clientMutationId: "sssd",
            fullName: Faker.name.firstName() + " " + Faker.name.lastName(),
            dob: chance.birthday(),
            gender: genGender(),
            customerId: customerId
          }
        }
      }
      request(headers(data), (error, response, body) => {
        if (!error && response.statusCode === 200) {
          
          console.log(JSON.stringify(body, null, 2))
        }else{
          console.log("body: ", body)
        }
      })
    })
    
  })
}
function createCustomer(callback){
  makeUser(userId => {
    
    const data = {
      query: `
        mutation CreateCustomer($input: CreateCustomerInput!) {
          createCustomer(input: $input) {
            customer{
              id
              user{
                id
              }
            }
          }
        }
      `,
      variables: {
        input: {
          clientMutationId: "sssd",
          fullName: Faker.name.firstName() + " " + Faker.name.lastName(),
          userId: userId
        }
      }
    }
    
    request(headers(data), (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const customerId = body.data.createCustomer.customer.id
        callback(customerId)
        
      }else{
        console.log(
          "createCustomerFailed: ",
          "error:",
          error,
          "statuscode:",
          response.statusCode
        )
        console.log("body: ", body)
      }
    })
  })
}

function createEmployee() {
  makeUser(userId => {
    
    const data = {
      query: `
        mutation CreateEmployee($input: CreateEmployeeInput!) {
          createEmployee(input: $input) {
            employee{
              user{
                id
              }
            }
          }
        }
      `,
      variables: {
        input: {
          clientMutationId: "sssd",
          fullName: Faker.name.firstName() + " " + Faker.name.lastName(),
          dob: chance.birthday(),
          salary: Faker.random.number(),
          gender: genGender() ,
          userId: userId
        }
      }
    }
    
    request(headers(data), (error, response, body) => {
      if (!error && response.statusCode == 200) {
        console.log(JSON.stringify(body, null, 2))
      }else{
        console.log("body: ", body)
      }
    })
  })
}

_.times(20, i => {
  //createPatient(_.random(0,2))
})

_.times(20, i => {
  createEmployee();
})
