// @flow
require('dotenv').config()

const Faker = require('faker/locale/id_ID')
const Lokka = require('lokka').Lokka
const Transport = require('lokka-transport-http').Transport
const _ = require("lodash")
// $FlowFixMe
const chance = require("chance").Chance()
const graphQLAPIUrl = process.env.GRAPHCOOL_URL
const superToken: any = process.env.GRAPHCOOL_SUPER_TOKEN
const mgmtAuth = require('./auth0Helper').mgmtAuth

function printLog(data){
  console.log(JSON.stringify(data, null, 2))
}
const client = new Lokka({
  transport: new Transport(graphQLAPIUrl, {
    Authorization: 'Bearer ' + superToken
  })
})

let tmpMgmt:any = null

function genGender (relationType){
  switch (relationType) {
    case 'SUAMI':
      return 'T1'
    case 'ISTRI':
      return 'T2'
    default:
      return chance.gender() === 'Female' ? 'T2' : 'T1'
  }
  
}

const exams = []

const fakeBranches = [
  {
    name: "SMD",
    city: "Samarinda",
    employeeCount: 2
  },
  {
    name: "MKS",
    city: "Makassar",
    employeeCount: 3
  }
]


function createExam(){

}



function createBranch(fakeBranch: any){
  //language=GraphQL
  const mutationQuery = `
    mutation asu ($input: CreateBranchInput!) {
      createBranch(input: $input) {
        branch{
          id
          employees{
            count
          }
          
        }
      }
    }
  `
  //Create Auth0 users
  return new Promise(resolve =>{
  generator(fakeUsers(fakeBranch.employeeCount), createAuth0User).then(Auth0users =>{
    
    // Create internal Graphcool Users
    generator(Auth0users , createUser).then((users)=>{
      
  
      const employees = users.map(user =>{
        return {
          fullName: user.fakeData.name,
          dob: chance.birthday(),
          salary: Faker.random.number(),
          gender: genGender('OTHERS') ,
          userId: user.gcUserid
        }
      })
  
      const vars = {
        "input": {
          "clientMutationId": "sssd",
          name: fakeBranch.name,
          city: fakeBranch.city,
          employees: employees
        }
      }
  
      client.query(mutationQuery, vars).then(data =>{
       
          resolve(data)
        
      })
    })
  })
  })
  // generator(fakeUsers(variables.needEmployees), createUser).then((users)=>{
  //   printLog(users)
  //
  //
  //
  //   // generator(users, createEmployee).then((employees)=>{
  //   //   printLog(employees)
  //   //
  //   //   const employeesIds = employees.map(el => {
  //   //     printLog(el.createEmployee.employee.id)
  //   //     return el.createEmployee.employee.id
  //   //   })
  //   //
  //   //
  //   // })
  // })
}

function createPatient(variables: any) {
  
  //language=GraphQL
  const mutationQuery = `
    mutation asu ($input: CreatePatientInput!) {
      createPatient(input: $input) {
        patient{
          id
        }
      }
    }
  `
  const customer = variables.createCustomer.customer
  const {needPatient} = variables
  
  needPatient.map(relationType => {
    const vars = {
      "input": {
        "clientMutationId": "sssd",
        fullName: variables.name+' '+relationType,
        dob: chance.birthday(),
        gender: genGender(relationType) ,
        customerId: customer.id
      }
    }
    client.query(mutationQuery, vars)
  })
 
}

function createEmployee(variables: any) {
  
  const user = variables.createUser.user
  //language=GraphQL
  const mutationQuery = `
    mutation asu ($input: CreateEmployeeInput!) {
      createEmployee(input: $input) {
        employee{
          id
        }
      }
    }
  `
  const vars = {
    "input": {
      "clientMutationId": "sssd",
      fullName: Faker.name.firstName() + " " + Faker.name.lastName(),
      dob: chance.birthday(),
      salary: Faker.random.number(),
      gender: genGender('OTHERS') ,
      userId: user.id
    }
  }
  return client.query(mutationQuery, vars)
}

function createCustomer(user: any){
  //language=GraphQL
  const needPatient = randomRelation()
  
  const patients = needPatient.map(patientRelation =>{
    return {
      fullName: Faker.name.firstName()+' '+user.fakeData.name,
      dob: chance.birthday(),
      gender: genGender(patientRelation) ,
    }
  })
  const mutationQuery = `
    mutation asu ($input: CreateCustomerInput!) {
      createCustomer(input: $input) {
        customer{
          id
        }
      }
    }
  `
  const vars = {
    input: {
      "clientMutationId": "sssd",
      fullName: user.fakeData.name,
      userId: user.gcUserid,
      patients: patients
    }
  }
  return new Promise(resolve =>{
    client.query(mutationQuery, vars).then(customer => {
      console.log('Customer: ', user.name, ' - ', user.email)
      resolve ({
        ...user,
        ...customer,
        needPatient: needPatient
      })
    })
  })
  
}

function createUser(user: any){
  
  //language=GraphQL
  const mutationQuery = `
     mutation asu ($input: SignupUserInput!) {
      createUser(input: $input) {
        user {
          id
          email
          
          
        }
      }
    }
  `
  const vars = {
    input: {
      clientMutationId: "sssd",
      email: user.email,
      avatarUrl: user.picture,
      authProvider: {
        auth0 :{
          idToken: user.id_token
        }
      }
    }
  }
  return new Promise(resolve => {
    client.query(mutationQuery, vars).then(gcUser => {
      resolve({
        gcUserid: gcUser.createUser.user.id,
        ...user
      })
    })
  })
 
}

function generator(datas, creator){
  return Promise.all(
    datas.map(element =>{
      return new Promise(resolve => {
        creator(element).then(result => resolve(result))
      })
    })
  )
}

function createAuth0User(fakeData: any){
  return new Promise(resolve => {
    tmpMgmt.management.users.create({ //create the user
      connection: 'sampah-masyarakat',
      email: fakeData.email,
      password: '1234',
      name: fakeData.name,
    }).then(user => {
      const param = {
        email: fakeData.email,
        username: fakeData.email,
        password: '1234',
        connection: user.identities[0].connection
      }
  
      tmpMgmt.auth0Auth.database.signIn(param).then(signinInfo => { 
        // then login and get id_token
        const newdata = {
          ...user,
          ...signinInfo,
          fakeData: fakeData
        }
        //console.log(newdata)
      
        resolve(newdata)
      })
    }).catch(function(e) {
      console.log("handled the error", e.message);
    })
  })
}

function fakeUsers (howMuch) {
  const x = []
  _.times(howMuch, i => {
    const fakeMe = fakeCard()
    x.push({
      email: fakeMe.email.toLowerCase(),
      name: fakeMe.name
    })
  })
  return x
}


function startGenerate(){
  mgmtAuth(mgmt => {
    tmpMgmt = mgmt
  
    
    
    generator(fakeUsers(10), createAuth0User).then(Auth0users =>{ //Create Auth0
      // Create internal Graphcool Users
      generator(Auth0users , createUser).then((users)=>{
        // Create Customers
        generator(users, createCustomer).then((customers)=>{
          printLog(customers)
          // // Create Patients
          // generator(customers, createPatient).then(patients =>{
          //   console.log('FINISH')
          // })
  
          generator(fakeBranches, createBranch).then(branches =>{
            printLog(branches)
          })
  
        })
      })
    })
  })
}


startGenerate()

function fakeCard() {
  const arrEmail = ['gmail.com', 'gmail.co.id', 'yahoo.com', 'hotmail.com']
  const sep = ['.','_', '']
  
  const fN = Faker.name.firstName()
  const lN = Faker.name.lastName()
  const email = fN + _.sample(sep) + lN + '@' + _.sample(arrEmail)
  
  return {
    name : fN + ' ' + lN,
    email
  }
}



function randomRelation(){
  const famArr = ['ME','ORTU', 'ANAK', 'SUAMI', 'ISTRI', 'OTHER']
  //const famShuffled = _.shuffle(famArr)
  return _.sampleSize(famArr, _.random(0,3))
  
}

function getEmployees() {
  //language=GraphQL
  return client.query(`
    {
      viewer{
        allEmployees(first:3){
          edges{
            node{
              fullName
            }
          }
        }
      }
    }
  `)
}
