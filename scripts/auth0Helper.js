// @flow weak
require('dotenv').config()
// $FlowFixMe
const request = require("request");

const AUTH0_DOMAIN: any = process.env.AUTH0_DOMAIN
const AUTH0_SEED_CLIENT_ID: any = process.env.AUTH0_SEED_CLIENT_ID
const AUTH0_SEED_CLIENT_SECRET: any = process.env.AUTH0_SEED_CLIENT_SECRET

const ManagementClient = require('auth0').ManagementClient;

const AuthenticationClient = require('auth0').AuthenticationClient;

export function genToken(callback: any){
  const options = {
    method: 'POST',
    url: `https://${AUTH0_DOMAIN}/oauth/token`,
    json: true,
    headers: {
      'content-type': 'application/json'
    },
    body: {
      "client_id":AUTH0_SEED_CLIENT_ID,
      "client_secret": AUTH0_SEED_CLIENT_SECRET,
      "audience":`https://${AUTH0_DOMAIN}/api/v2/`,
      "grant_type":"client_credentials"
    }
  };
  
  request(options, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      callback(body.access_token)
    } else {
      console.log("GEN TOKEN ERROR: ", body)
    }
    
  });
}


export function mgmtAuth(callback: any) {
  genToken(token => {
    const management = new ManagementClient({
      token: token,
      domain: process.env.AUTH0_DOMAIN
    });
    
    const auth0Auth = new AuthenticationClient({
      domain: process.env.AUTH0_DOMAIN,
      clientId: process.env.AUTH0_CLIENT_ID
    });
    callback({
      token,
      management,
      auth0Auth
    })
  
  })
}
