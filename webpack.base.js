// @flow

require('dotenv').config()
const path = require("path")
const RelayCompilerWebpackPlugin = require('relay-compiler-webpack-plugin')
const HtmlWebpackPlugin = require("html-webpack-plugin")
const webpack = require("webpack")
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')
const publicPath = "/"
console.log('asuuuu', path.resolve(__dirname, "node_modules/semantic-ui-css/themes"))
module.exports = function() {
  return {
    entry: {
      vendor: [
        "react",
        "react-dom",
        "react-relay",
        "react-router",
        //"react-router-relay",
        //"auth0-lock",
        "semantic-ui-react",
        "react-table",
        "react-datepicker",
        "react-jsonschema-form",
        "react-hot-loader",
        "moment"
      ]
    },
    output: {
      path: path.join(__dirname, "./dist"),
      filename: "[hash:5].[name].js",
      publicPath: publicPath,
      //sourceMapFilename: "[name].map"
    },
    resolve: {
      extensions: ['.js'],
      modules: [path.resolve(__dirname, './client'), 'node_modules'],
    },
    module: {
      rules: [
        {
          test: /\.js?$/,
          use: ["babel-loader"],
          exclude: [/node_modules/]
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader", {
            loader: 'resolve-url-loader',
            options:{
              debug: true
            }
          }],

          exclude: {
            test   : path.resolve(__dirname, "node_modules"),
            exclude: [
              path.resolve(__dirname, "node_modules/react-table"),
              //path.resolve(__dirname, "node_modules/semantic-ui-css"),
              path.resolve(__dirname, "node_modules/react-datepicker/dist")
            ]
          }
        },
        {
          test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
          use: [
            {
              loader: "url-loader",
              options: {
                limit: 1000,
                name: "assets/[hash].[ext]"
              }
            }
          ],
          exclude: {
            test   : path.resolve(__dirname, "node_modules"),
            exclude: [
              //path.resolve(__dirname, "node_modules/semantic-ui-css")
            ]
          }
        },
        {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "file-loader"
        }
      ]
    },
    plugins: [
      new webpack.EnvironmentPlugin(["NODE_ENV"]),

      new webpack.optimize.CommonsChunkPlugin({
        name: ["polyfills", "vendor"].reverse()
      }),

      new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en|id)$/),

      // new RelayCompilerWebpackPlugin({
      //       schema: path.resolve(__dirname, './server/data/graphcoolschema.graphql'), // or schema.json
      //       src: path.resolve(__dirname, './client'),
      //     }),

      new webpack.DefinePlugin({
        __SCAPHOLD_URL__: JSON.stringify(process.env.SCAPHOLD_URL),
        __SCAPHOLD_WS_URL__: process.env.SCAPHOLD_WS_URL,
        __GRAPHCOOL_URL__: JSON.stringify(process.env.GRAPHCOOL_URL),
        __GRAPHCOOL_WS_URL__: process.env.GRAPHCOOL_WS_URL,
        __AUTH0_DOMAIN__ : JSON.stringify(process.env.AUTH0_DOMAIN),
        __AUTH0_CLIENT_ID__ : JSON.stringify(process.env.AUTH0_CLIENT_ID)
      }),
      new HtmlWebpackPlugin({
        mobile: true,
        favicon: './client/assets/icofiles/favicon.ico',
        template: path.resolve(__dirname, './client/index.html'),
        chunksSortMode: "dependency"
      }),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'defer'
      })
    ]
  }
}
