import _ from 'lodash';

const config = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3001,
  graphql: {
    port: 9003
  }
};

export default _.extend(config, require(`./${config.env}`).default);
