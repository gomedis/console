
require('dotenv').config()

const express = require("express")
const compression = require('compression')
const path = require("path")
const fs = require('fs')
// const WebpackDevServer = require("webpack-dev-server")
// const webpackConfig = require("../webpack.config")



const https = require('https');
const httpsPort = 7443;

let options = {
  key: fs.readFileSync(path.resolve(__dirname, './cert', 'key.txt')),
  cert: fs.readFileSync(path.resolve(__dirname, './cert', 'crt.txt'))
};


const oneWeek = 86400000*7;
//console.log(__dirname)
const app = express()

// app.all('*', function(req, res, next){
//
//   if (req.secure) {
//     console.log('req is secure')
//
//     return next();
//   }
//   console.log('req not secure redirectting')
//   //res.redirect('https://'+ req.headers.host +req.url);
//    res.redirect('https://'+req.hostname+':'+httpsPort+req.url);
// });


app.use(compression())


app.use(express.static('dist', {
  maxage: oneWeek
}));

app.get('*', function(req, res, next){
  res.set({'Cache-Control': 'public, max-age=' + oneWeek});
  res.sendFile(path.resolve(__dirname, '../dist', 'index.html'));
});

// const secureServer = https.createServer(options, app)
//
// secureServer.listen(httpsPort, "0.0.0.0", () =>
//   console.log(
//     `SECURE SERVER RUNNING ON http://0.0.0.0:${httpsPort}`
//   ));
//


//app.use("*", express.static(path.join(__dirname, "../dist")))

app.listen(process.env.PRODUCTION_PORT, "0.0.0.0", () =>
  console.log(
    `PRODUCTION SERVER RUNNING ON http://0.0.0.0:${process.env.PRODUCTION_PORT}`
  )
)
