function buildConfig(env) {
  if (env === undefined) {
    return require("./webpack." + "dev" + ".config.js")("dev")
  } else {
    return require("./webpack." + env + ".config.js")(env)
  }
}

module.exports = buildConfig
