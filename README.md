# README #

Repo ini untuk Admin Dashboard. Pada dasarnya repo ini berfungsi untuk karyawan lab memanage, monitoring, analitic keseluruhan project. dashboard ini dirancang untuk skalabilitas dimasa depan.

Karena repo ini digunakan untuk karyawan memanage customer/karyawan maka tampilan mobile (Responsive View) tidak diperlukan karena pasti diakses menggunakan dekstop. juga compability untuk IE juga dapat diabaikan

Server API untuk aplikasi ini menggunakan GraphQL (bukan REST). karena nanti aplikasi mobile juga akan "talk" menggunakan GraphQL. Endpoint yang digunakan adalah GraphQl As Service dari www.scalphold.io


### Yang dimaksud karyawan disini adalah ###
* Dokter
* Petugas Lab
* Manager Lab
* Petugas Lapangan (kalau diperlukan)

### Fungsi Utama ###
* User Auth
* CRUD karyawan
* CRUD Team. Untuk skalabilitas jika Lab medik buka di beberapa kota atau mengajak kerjasama medikal lab dari kota lain (semacam Franchise)
* Memanage Role & Authorization Karyawan
* karyawan yg mempunyai authorization tertentu dapat memonitor semua customer melalui interactive maps
* Manage/Monitor/Analitic/Chat/Message kepada Pasien/Customer
* Manage Tipe-tipe Test

### Teknologi yang digunakan ###
* UI: ReactJS
* CSS/UI Component: http://react.semantic-ui.com/
* GraphQL Client: react-relay
* React-router: react-router-relay
* Webpack 2
* JS Formatter: prettier/prettier
* Transpilation: Babel ES6 Next
* Linter: EsLint
* Auth broker: menggunakan Auth0
* JS Type Check: FlowType
* IDE: saya rekomendasikan menggunakan WebStorm 2017

### How do I get set up? ###

* Pertama Git, NodeJS dan Yarn (lebih recommended dari pada NPM) harus terinstall terlebih dahulu di sistem anda
* clone repo ini
* yarn install
* setelah selesai: yarn start
* lalu buka localhost:3001